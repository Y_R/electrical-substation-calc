package MathCalck;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class GenerateMath4 extends MathExtend {

    private Context context;
    private File file;
    private Map<String,Object> mapValue = new HashMap<>();


    public GenerateMath4(Context _context, Map<String, Object> _mapValue){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
        context = _context;
        mapValue = _mapValue;
        File dir = new  File(path);
        dir.mkdir();
        file  = new File(path + "/file.html");
        if(file.exists())
            file.delete();
        else{
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            this.library(_context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String createBodyHTML(String _html){

        Double Saparati = (Double)mapValue.get("editText_Voltmetur_FazaB_4") + (Double)mapValue.get("editText_Aktivna_FazaB_4") + (Double)mapValue.get("editText_Reaktivna_FazaB_4");

        Double Rpr = ((Double)mapValue.get("editText_KlasNaTochnost_4") * (Double)mapValue.get("editText_NominalnaMoshtnost_4")) / (3*Saparati);
        Double Sizch = ((Double)mapValue.get("editText_specifichnoSup_4") * (Double)mapValue.get("editText_duljina_4")) / Rpr;

        Double S;
        if(Sizch <= 1.5){
            S= 1.5;
        }
        else if(Sizch > 1.5 && Sizch <= 2.5){
            S = 2.5;
        }
        else if(Sizch > 2.5 && Sizch <= 4){
            S = Double.valueOf(4);
        }
        else if(Sizch > 4 && Sizch <= 6){
            S = Double.valueOf(6);
        }
        else if(Sizch > 6 && Sizch <= 10){
            S = Double.valueOf(10);
        }
        else{
            S = Double.valueOf(16);
        }


        if(getLanguage() == 0) {
            _html += "<body>\n" +
                    "<p>4.Избор на напреженов трансформатор</p>\n" +
                    "<p>Избираме напреженов трансформатор тип " + mapValue.get("editText_TransName_4") + "</p>\n" +
                    "<p>Параметри на избрания напреженов трансформатор:</p>\n" +
                    "<p>Номинално напрежение $U_n= " + generateStr((Double) mapValue.get("editText_Un_4")) + " kV$</p>\n" +
                    "<p>Коефициент на трансформация $k_тр= $" + mapValue.get("editText_KoeficientTrans_4") + "</p>\n" +
                    "<p>Номинална пределна мощност $S_n_2= " + generateStr((Double) mapValue.get("editText_NominalnaMoshtnost_4")) + " VA$</p>\n" +
                    "<p>Клас на точност N= " + generateStr((Double) mapValue.get("editText_KlasNaTochnost_4")) + "</p>\n" +
                    "<p>Избор по клас на точност: </p>\n" +
                    "$$N≤N_\\text \"доп.\"$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_KlasNaTochnost_4")) + " ≤ 0,5 $$\n" +
                    "<p>Избор по изолационна якост: </p>\n" +
                    "$$U_n≥U_\\text \"max.раб.\"$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Un_4")) + " kV ≥ " + generateStr((Double) mapValue.get("editText_MaksNapr_4")) + " kV$$\n" +
                    "<p>Вторично натоварване:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>пр</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>ΔU.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>3</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) mapValue.get("editText_KlasNaTochnost_4")) + " . " + generateStr((Double) mapValue.get("editText_NominalnaMoshtnost_4")) + "</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>3 . "+generateStr((Double)Saparati)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = " + generateStr((Double) Rpr, 3) + " Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Сечението: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn> ρ </mn>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\t\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>изч</mi>\n" +
                    "\t\t\t</msub>\t\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>пр</mi>\n" +
                    "\t\t\t</msub>\t\t\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn> " + generateStr((Double) mapValue.get("editText_specifichnoSup_4"), 3) + " </mn>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\t\n" +
                    "\t<mo> " + generateStr((Double) mapValue.get("editText_duljina_4")) + " </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t<mo> " + generateStr((Double) Rpr, 3) + " </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = " + generateStr((Double) Sizch, 3) + " </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>mm</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\t\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>избираме $S= " + generateStr((Double) S, 3) + " mm^2$</p>" +
                    "</body>";
        }
        else{
            _html += "<body>\n" +
                    "<p>4.Potential transformer selection</p>\n" +
                    "<p>We choose potential transformer type "+mapValue.get("editText_TransName_4")+"</p>\n" +
                    "<p>Parameters of the selected potential transformer:</p>\n" +
                    "<p>Rated voltage $U_n= "+generateStr((Double)mapValue.get("editText_Un_4"))+" kV$</p>\n" +
                    "<p>Turns ratio  $k_turns= $"+mapValue.get("editText_KoeficientTrans_4")+"</p>\n" +
                    "<p>Rated power $S_n_2= "+generateStr((Double)mapValue.get("editText_NominalnaMoshtnost_4"))+" VA$</p>\n" +
                    "<p>Accuracy class N= "+generateStr((Double)mapValue.get("editText_KlasNaTochnost_4"))+"</p>\n" +
                    "<p>Accuracy class condition: </p>\n" +
                    "$$N≤N_\\text \"allowable\"$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_KlasNaTochnost_4"))+" ≤ 0,5 $$\n" +
                    "<p>Insulation strength condition: </p>\n" +
                    "$$U_n≥U_\\text \"max.operational\"$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Un_4"))+" kV ≥ "+generateStr((Double)mapValue.get("editText_MaksNapr_4"))+" kV$$\n" +
                    "<p>Secondary load:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>referred</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>ΔU.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>3</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)mapValue.get("editText_KlasNaTochnost_4"))+" . "+generateStr((Double)mapValue.get("editText_NominalnaMoshtnost_4"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>3 . 72</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = "+generateStr((Double)Rpr,3)+" Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Cross section: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn> ρ </mn>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\t\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>calculated</mi>\n" +
                    "\t\t\t</msub>\t\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>referred</mi>\n" +
                    "\t\t\t</msub>\t\t\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn> "+generateStr((Double)mapValue.get("editText_specifichnoSup_4"),3)+" </mn>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\t\n" +
                    "\t<mo> "+generateStr((Double)mapValue.get("editText_duljina_4"))+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t<mo> "+generateStr((Double)Rpr,3)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = "+generateStr((Double)Sizch,3)+" </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>mm</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\t\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>we choose $S= "+generateStr((Double)S,3)+" mm^2$</p>"+
                    "</body>";
        }

        return _html;
    }

    @Override
    public File getFile() {
        return file;
    }
}
