package MathCalck;

import android.content.Context;
import android.os.Environment;

import com.itextpdf.tool.xml.parser.state.DoubleQuotedAttrValueState;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class GenerateMath3 extends MathExtend {

    private Context context;
    private File file;
    private Map<String,Object> mapValue = new HashMap<>();


    public GenerateMath3(Context _context, Map<String, Object> _mapValue){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
        context = _context;
        mapValue = _mapValue;
        File dir = new  File(path);
        dir.mkdir();
        file  = new File(path + "/file.html");
        if(file.exists())
            file.delete();
        else{
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            this.library(_context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String createBodyHTML(String _html){


        Double Saparat = (Double)mapValue.get("editText_Ampermetur_FazaB_3") + (Double)mapValue.get("editText_Aktivna_FazaB_4") + (Double)mapValue.get("editText_Reaktivna_FazaB_4") + (Double)mapValue.get("editText_Reaktivna_FazaB_42") + (Double)mapValue.get("editText_Reaktivna_FazaB_43");

        Double Rpr = (((Double)mapValue.get("editText_Pvtorichen_3") - (Saparat + Math.pow(( Double)mapValue.get("editText_Ivtorichen_3"),2) * (Double)mapValue.get("editText_Rk_3"))) / Math.pow((Double)mapValue.get("editText_Ivtorichen_3"),2));
        Double Lizch = (Double)mapValue.get("editText_kt_3") * (Double)mapValue.get("editText_ksh_3") * (Double)mapValue.get("editText_L_3");
        Double Sprizch= (Double)mapValue.get("editText_ro_3") * (Lizch / Rpr);

        Double Spr;
        if(Sprizch <= 2.5){
            Spr = 2.5;
        }
        else if(Sprizch > 2.5 && Sprizch <= 4){
            Spr = Double.valueOf(4);
        }
        else if(Sprizch > 4 && Sprizch <= 6){
            Spr = Double.valueOf(6);
        }
        else if(Sprizch > 6 && Sprizch <= 10){
            Spr = Double.valueOf(10);
        }
        else{
            Spr = Double.valueOf(16);
        }




        Double Rprdei = (Double)mapValue.get("editText_ro_3") * ((Lizch / Spr));
        Double S2 = Saparat + (Rprdei + (Double)mapValue.get("editText_Rk_3")) * Math.pow((Double)mapValue.get("editText_Ivtorichen_3"),2);

        Double t = (Double)mapValue.get("editText_IzklReleinaZasht_3") + (Double)mapValue.get("editText_MinIzklReleinaZasht_3")/1000;
        Double Ta = (Double)mapValue.get("editText_xr_3") / (2 * 3.14 * 50);
        Double natural_e = Math.pow(2.71828,-((Double)mapValue.get("editText_MinIzklReleinaZasht_3")/Ta));
        Double Ky = 1 + natural_e;
        Double Iy = Math.sqrt(2) * (Double)mapValue.get("editText_Iprim_K2_3") * Ky;
        Double Bk = (Math.pow((Double)mapValue.get("editText_I8_K2_3"),2) * t) + (Math.pow((Double)mapValue.get("editText_Iprim_K2_3"),2) * Ta);


        if(getLanguage() == 0) {
            _html += "<body>\n" +
                    "<p>1.Изчисляване на токовете на късо съединение</p>\n" +
                    "<p>"+
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>т.к.с., kA</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>$$I_0,2$$</th>\n" +
                    "    <th>$$I_∞$$</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>K2</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Iprim_K2_3"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_I02_K2_3"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_I8_K2_3"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+
                    "<p>3.Избор на токов трансформатор</p>\n" +
                    "<p>Избираме токов трансформатор тип " + mapValue.get("editText_name_3") + "</p>\n" +
                    "<p>Избор по продължително нагряване: </p>\n" +
                    "$$I_n= " + generateStr((Double) mapValue.get("editText_A_3")) + " A$$\n" +
                    "<p>Условието: </p>\n" +
                    "$$I_\\text \"n,тр.токов\" ≥ I_n$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Ipurvchen_3")) + " A ≥ " + generateStr((Double) mapValue.get("editText_A_3")) + " A$$\n" +
                    "<p>Параметри на избрания трансформатор:</p>\n" +
                    "<p>Номинално напрежение: $U_n= " + generateStr((Double) mapValue.get("editText_Un_3")) + " kV$</p>\n" +
                    "<p>Първичен ток: $I_n= " + generateStr((Double) mapValue.get("editText_Ipurvchen_3")) + " A$</p>\n" +
                    "<p>Вторичен ток: $I_2_n= " + generateStr((Double) mapValue.get("editText_Ivtorichen_3")) + " A$</p>\n" +
                    "<p>Вторична мощност: $S_2_n= " + generateStr((Double) mapValue.get("editText_Pvtorichen_3")) + " VA$</p>\n" +
                    "<p>Ток на термична устойчивост за 1 секунда: $I_t= " + generateStr((Double) mapValue.get("editText_Itermust_3")) + " $kA/s</p>\n" +
                    "<p>Клас на точност N= " + generateStr((Double) mapValue.get("editText_klasTochnost_3")) + " </p>\n" +
                    "<p>Избор по клас на точност: </p>\n" +
                    "$$N≤N_\\text \"доп.\"$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_klasTochnost_3")) + " ≤ 0,5 $$\n" +
                    "<p>Избор по вторично натоварване: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>пр</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>-</mo>\n" +
                    "\t\t\t<mo>(</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>ΣS</mn>\n" +
                    "\t\t\t<mi>ап</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<mi>I</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>2</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t\t<mo>.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>)</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>I</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>2</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo>=</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) mapValue.get("editText_Pvtorichen_3")) + "</mo>\n" +
                    "\t\t\t<mo> - </mo>\n" +
                    "\t\t\t<mo>(</mo>\n" +
                    "\t\t\t<mo>"+generateStr((Double)Saparat)+"</mo>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>" + generateStr((Double) mapValue.get("editText_Ivtorichen_3")) + "</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mo>" + generateStr((Double) mapValue.get("editText_Rk_3")) + "</mo>\n" +
                    "\t\t\t<mo>)</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>" + generateStr((Double) mapValue.get("editText_Ivtorichen_3")) + "</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +


                    "<mo> = " + generateStr((Double) Rpr, 3) + " Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>l</mn>\n" +
                    "\t<mi>изч</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>k</mn>\n" +
                    "\t<mi>т</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>k</mn>\n" +
                    "\t<mi>сх</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mn>l</mn>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mo>" + generateStr((Double) mapValue.get("editText_kt_3")) + "</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>" + generateStr((Double) mapValue.get("editText_ksh_3")) + "</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mn>" + generateStr((Double) mapValue.get("editText_L_3")) + "</mn>\n" +

                    "\t<mo> = " + generateStr((Double) Lizch) + " m</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Избираме минимално допустимо сечение в $mm^2$: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>пр</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn>ρ</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>изч</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>пр</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn>" + generateStr((Double) mapValue.get("editText_ro_3"), 3) + "</mn>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) Lizch) + "</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) Rpr) + "</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = " + generateStr((Double) Sprizch, 3) + "</mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>mm</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p> избираме $S_\\text \"пр\"= "+generateStr((Double) Spr, 3)+" mm^2$</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>пр,действ</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn>ρ</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>изч</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>пр</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn>" + generateStr((Double) mapValue.get("editText_ro_3"), 3) + "</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) Lizch) + "</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>" + generateStr((Double) Spr, 3) + "</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = " + generateStr((Double) Rprdei, 3) + " Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Изчисляване на действителното вторично натоварване: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>ΣS</mn>\n" +
                    "\t<mi>ап</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo> ( </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>пр,действ</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ) </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>2n</mi>\n" +
                    "\t</msubsup>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr((Double)Saparat)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo> ( </mo>\n" +
                    "\t<mo>" + generateStr((Double) Rprdei) + "</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mn>" + generateStr((Double) mapValue.get("editText_Rk_3")) + "</mn>\n" +
                    "\t<mo> ) </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mi>" + generateStr((Double) mapValue.get("editText_Ivtorichen_3")) + "</mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t</msup>\n" +

                    "\t<mo> = " + generateStr((Double) S2) + " VA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Условието </p>\n" +
                    "$$S_2_n≥S_2$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Pvtorichen_3")) + " VA≥ " + generateStr((Double) S2) + " VA$$\n" +
                    "<p>Проверка по динамична устойчивост(ако няма ток на динамична устойчивост избираме ток на термична устойчивост): </p>\n" +
                    "$$i_\\text \"din\"≥i_y$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Itermust_3")) + " kA≥ " + generateStr((Double) Iy) + " kA$$\n" +
                    "<p>Проверка по термична устойчивост</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>доп</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>≥</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Itermust_3")) + "^2 . " + generateStr((Double) mapValue.get("editText_IzklReleinaZasht_3")) + " ≥ " + generateStr((Double) Bk) + " kA^2 .s$$\n" +
                    "$$ " + generateStr(Math.pow((Double) mapValue.get("editText_Itermust_3"), 2)) + " kA^2.s ≥ " + generateStr((Double) Bk) + " kA^2.s$$" +
                    "</body>";
        }
        else{
            _html += "<body>\n" +
                    "<p>1.Short-circuit calculations</p>\n" +
                    "<p>"+
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>short-circuit current, kA</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>$$I_0,2$$</th>\n" +
                    "    <th>$$I_∞$$</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>K2</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Iprim_K2_3"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_I02_K2_3"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_I8_K2_3"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+
                    "<p>3.short-circuit current</p>\n" +
                    "<p>We select current transformer type "+mapValue.get("editText_name_3")+"</p>\n" +
                    "<p>Continuous heating condition: </p>\n" +
                    "$$I_n= "+generateStr((Double)mapValue.get("editText_A_3"))+" A$$\n" +
                    "<p>The condition: </p>\n" +
                    "$$I_\\text \"curr.tr.\" ≥ I_n$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Ipurvchen_3"))+" A ≥ "+generateStr((Double)mapValue.get("editText_A_3"))+" A$$\n" +
                    "<p>Parameters of the chosen current transformer:</p>\n" +
                    "<p>Rated voltage: $U_n= "+generateStr((Double)mapValue.get("editText_Un_3"))+" kV$</p>\n" +
                    "<p>Primary current: $I_n= "+generateStr((Double)mapValue.get("editText_Ipurvchen_3"))+" A$</p>\n" +
                    "<p>Secondary current: $I_2_n= "+generateStr((Double)mapValue.get("editText_Ivtorichen_3"))+" A$</p>\n" +
                    "<p>Secondary power: $S_2_n= "+generateStr((Double)mapValue.get("editText_Pvtorichen_3"))+" VA$</p>\n" +
                    "<p>Rated short-time withstand current for 1 sec: $I_t= "+generateStr((Double)mapValue.get("editText_Itermust_3"))+" $kA/s</p>\n" +
                    "<p>Accuracy class N= "+generateStr((Double)mapValue.get("editText_klasTochnost_3"))+" </p>\n" +
                    "<p>Accuracy class condition: </p>\n" +
                    "$$N≤N_\\text \"доп.\"$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_klasTochnost_3"))+" ≤ 0,5 $$\n" +
                    "<p>Secondary load condition: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>referred</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>-</mo>\n" +
                    "\t\t\t<mo>(</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>ΣS</mn>\n" +
                    "\t\t\t<mi>apparatus</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<mi>I</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>2</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t\t<mo>.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>)</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>I</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>2</mn>\n" +
                    "\t\t\t<mi>2n</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo>=</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)mapValue.get("editText_Pvtorichen_3"))+"</mo>\n" +
                    "\t\t\t<mo> - </mo>\n" +
                    "\t\t\t<mo>(</mo>\n" +
                    "\t\t\t<mo>5</mo>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>"+generateStr((Double)mapValue.get("editText_Ivtorichen_3"))+"</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mo>"+generateStr((Double)mapValue.get("editText_Rk_3"))+"</mo>\n" +
                    "\t\t\t<mo>)</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>"+generateStr((Double)mapValue.get("editText_Ivtorichen_3"))+"</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +


                    "<mo> = "+generateStr((Double)Rpr,3)+" Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>l</mn>\n" +
                    "\t<mi>calculated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>k</mn>\n" +
                    "\t<mi>load</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>k</mn>\n" +
                    "\t<mi>configuration</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mn>l</mn>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_kt_3"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_ksh_3"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_L_3"))+"</mn>\n" +

                    "\t<mo> = "+generateStr((Double)Lizch)+" m</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We choose minimal allowable section in $mm^2$: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>referred</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn>ρ</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>calculated</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>R</mn>\n" +
                    "\t\t\t<mi>referred</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_ro_3"),3)+"</mn>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)Lizch)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)Rpr)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = "+generateStr((Double)Sprizch,3)+"</mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>mm</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p> we choose $S_\\text \"referred\"= "+generateStr((Double)Spr,3)+" mm^2$</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>referred,actual</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mn>ρ</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>l</mn>\n" +
                    "\t\t\t<mi>calculated</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>referred</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_ro_3"),3)+"</mn>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)Lizch)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr((Double)Spr,3)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +

                    "\t<mo> = "+generateStr((Double)Rprdei,3)+" Ω</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Calculation of actual secondary load:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>ΣS</mn>\n" +
                    "\t<mi>apparatus</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo> ( </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>referred,actual</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>R</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ) </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>2n</mi>\n" +
                    "\t</msubsup>\n" +

                    "\t<mo> = </mo>\n" +
                    "\t<mo>5</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo> ( </mo>\n" +
                    "\t<mo>"+generateStr((Double)Rpr)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_Rk_3"))+"</mn>\n" +
                    "\t<mo> ) </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mi>"+generateStr((Double)mapValue.get("editText_Ivtorichen_3"))+"</mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t</msup>\n" +

                    "\t<mo> = "+generateStr((Double)S2)+" VA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>The condition </p>\n" +
                    "$$S_2_n≥S_2$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Pvtorichen_3"))+" VA≥ "+generateStr((Double)S2)+" VA$$\n" +
                    "<p>Short-time withstand current condition( if not available we choose the rated short-time withstand current):</p>\n" +
                    "$$i_\\text \"din\"≥i_\\text \"pick−up\"$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Itermust_3"))+" kA≥ "+generateStr((Double)Iy)+" kA$$\n" +
                    "<p>Rated short-time withstand current condition</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>rated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo>≥</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Itermust_3"))+"^2 . "+generateStr((Double)mapValue.get("editText_IzklReleinaZasht_3"))+" ≥ "+generateStr((Double)Bk)+" kA^2 .s$$\n" +
                    "$$ "+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_3"),2))+" kA^2.s ≥ "+generateStr((Double)Bk)+" kA^2.s$$"+
                    "</body>";
        }
        return _html;
    }

    @Override
    public File getFile() {
        return file;
    }

}
