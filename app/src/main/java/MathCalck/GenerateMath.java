package MathCalck;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.*;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;

public class GenerateMath extends MathExtend{

    private Context context;
    private File file;
    private Map<String, Object> mapValue = new HashMap<>();

    public GenerateMath(Context _context, Map<String, Object> _mapValue){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
        context = _context;
        mapValue = _mapValue;
        File dir = new  File(path);
        dir.mkdir();
        file  = new File(path + "/file.html");
        if(file.exists())
            file.delete();
        else{
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            this.library(_context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String createBodyHTML(String _html) throws IOException {
        Double S_kk1_prim = java.lang.Math.sqrt(3) * (Double) mapValue.get("editText_Usrednovn") * (Double)mapValue.get("editText_Iprim_K1");
        Double S_kk1_02 = java.lang.Math.sqrt(3) * (Double) mapValue.get("editText_Usrednovn") * (Double)mapValue.get("editText_I02_K1");
        Double S_kk1_8 = java.lang.Math.sqrt(3) * (Double) mapValue.get("editText_Usrednovn") * (Double)mapValue.get("editText_I8_K1");
        if((Double)mapValue.get("editText_Uk_Trans1") == 0
        || (Double)mapValue.get("editText_Uk_Trans2") == 0
        || (Double) mapValue.get("editText_Usrednosr") ==0
        || (Double)mapValue.get("editText_Sn_Trans1") == 0
        || (Double)mapValue.get("editText_Sn_Trans2") == 0){
            return "Въведена е стойност, с която не може да се извършат съответните изчисления!";
        }

        Double S_kt1 = (100/(Double)mapValue.get("editText_Uk_Trans1")) * (Double)mapValue.get("editText_Sn_Trans1");
        Double S_kt2 = (100/(Double)mapValue.get("editText_Uk_Trans2")) * (Double)mapValue.get("editText_Sn_Trans2");
        Double S_ktsuma = S_kt1 + S_kt2;
        Double S_ksuma1prim = (S_ktsuma * S_kk1_prim) / (S_ktsuma + S_kk1_prim);
        Double S_ekvprim = S_ksuma1prim + (Double) mapValue.get("editText_Skobr");
        Double I_k2prim = S_ekvprim / (java.lang.Math.sqrt(3) * (Double) mapValue.get("editText_Usrednosr"));
        Double S_ksum102 = (S_ktsuma * S_kk1_02) / (S_ktsuma + S_kk1_02);
        Double S_ekv02 = S_ksum102 + (Double) mapValue.get("editText_Skobr");
        Double I_k202 = S_ekv02 / (Math.sqrt(3) * (Double) mapValue.get("editText_Usrednosr"));
        Double S_ksum18 = (S_ktsuma * S_kk1_8) / (S_ktsuma + S_kk1_8);
        Double S_ekv8 = S_ksum18 + (Double) mapValue.get("editText_Skobr");
        Double I_k28 = S_ekv8 / (Math.sqrt(3) * (Double) mapValue.get("editText_Usrednosr"));

        Double S_ksum1prim_ = (S_kt2 * S_kk1_prim) / (S_kt2 + S_kk1_prim);
        Double S_evkprim_ = S_ksum1prim_ + (Double)mapValue.get("editText_Skobr");
        Double I_k2prim_ = S_evkprim_ / (Math.sqrt(3) * (Double)mapValue.get("editText_Usrednosr"));
        Double S_ksum102_ = (S_kt2 * S_kk1_02) / (S_kt2 + S_kk1_02);
        Double S_ekv02_ = S_ksum102_ + (Double)mapValue.get("editText_Skobr");
        Double I_k202_ = S_ekv02_ / (Math.sqrt(3) * (Double) mapValue.get("editText_Usrednosr"));
        Double S_ksum18_ = (S_kt2 * S_kk1_8) / (S_kt2 + S_kk1_8);
        Double S_ekv8_ = S_ksum18_ + (Double)mapValue.get("editText_Skobr");
        Double I_k28_ = S_ekv8_ / (Math.sqrt(3) * (Double)mapValue.get("editText_Usrednosr"));

        if(getLanguage() == 0){
            _html += "<body>\n" +
                    "<p>1.Изчисляване на токовете на късо съединение</p>\n" +
                    "<p>Избираме Uср:</p>\n" +
                    "<p>За Uн, ср.н. ,kV = "+mapValue.get("editText_Unsrn")+"kV съответно за Uсредно, ср.н. ,kV = "+mapValue.get("editText_Usrednosr")+"kV</p>\n" +
                    "<p>За Uн, в.н. ,kV = "+mapValue.get("editText_Unvn")+"kV съответно за Uсредно, в.н. ,kV = "+mapValue.get("editText_Usrednovn")+"kV</p>\n" +
                    "<p>Заместваща схема:  </p>\n" +
                    "<img src='"+ "file:///android_asset/1-1.png" +"' style='width:100px;height:180px;'/> "+
                    "<p>Избираме силов трансформатор 1 с показатели: "+mapValue.get("editText_NameTrans1")+" Sn="+mapValue.get("editText_Sn_Trans1")+"MVA Un="+mapValue.get("editText_Un_Trans1")+" I0="+mapValue.get("editText_I0_Trans1")+",% Uk,%="+mapValue.get("editText_Uk_Trans1")+",% ΔPст="+mapValue.get("editText_Pst_Trans1")+"kW ΔPcu="+mapValue.get("editText_Pcu_Trans1")+"kW </p>\n" +
                    "<p>Избираме силов трансформатор 2 с показатели: "+mapValue.get("editText_NameTrans2")+" Sn="+mapValue.get("editText_Sn_Trans2")+"MVA Un="+mapValue.get("editText_Un_Trans2")+" I0="+mapValue.get("editText_I0_Trans1")+",% Uk,%="+mapValue.get("editText_Uk_Trans2")+",% ΔPст="+mapValue.get("editText_Pst_Trans2")+"kW ΔPcu="+mapValue.get("editText_Pcu_Trans2")+"kW </p>\n" +
                    "<p>Паралелна работа:</p>\n" +
                    "<p>Изчисляване на пределно пропускателната мощност на системата в точка К1:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>ср</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    //calculation
                    "\t<mo>√3 . "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Iprim_K1"))+"</mo>\n" +
                    "\t<mo> = "+ generateStr(S_kk1_prim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>ср</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    //calculation
                    "\t<mo> = </mo>\n" +
                    "\t<mo>√3 . "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_I02_K1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kk1_02)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>ср</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>√3 "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_I8_K1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kk1_8)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Изчисляване на пределно пропускателната мощност на силовия трансформатор 1:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<mn>k,т.1</mn>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>u</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>,%</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>n</mi>\n" +
                    "\t</msub>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    ""+generateStr((Double)mapValue.get("editText_Uk_Trans1"))+""+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Sn_Trans1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kt1)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Изчисляване на пределно пропускателната мощност на силовия трансформатор 2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<mn>k,т.2</mn>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>u</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>,%</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>n</mi>\n" +
                    "\t</msub>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    ""+generateStr((Double)mapValue.get("editText_Uk_Trans2"))+""+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Sn_Trans2"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kt2)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,тΣ</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,т1</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,т2</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = "+generateStr(S_kt1)+" + "+generateStr(S_kt2)+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ktsuma)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на</p>\n" +
                    "<p align='center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>.</mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo>= "+generateStr(S_ksuma1prim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Заместваща схема:</p>\n" +
                    "<img src='"+ "file:///android_asset/1-2.png" +"' style='width:100px;height:180px;'/>"+
                    "<img src='"+ "file:///android_asset/1-3.png" +"' style='width:150px;height:100px;'/>"+
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksuma1prim)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekvprim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>√3.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ekvprim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo> √3 . "+generateStr((Double)mapValue.get("editText_Usrednosr"))+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k2prim)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum102)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum102)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv02)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ekv02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>√3 . "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k202)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,тΣ</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,тΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_8)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_8)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum18)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum18)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv8)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr(S_ekv8)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k28)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Разделна работа:</p>\n" +
                    "<p>Заместваща схема:</p>\n" +
                    "<img src='"+ "file:///android_asset/1-4.png" +"' style='width:150px;height:100px;'/>"+
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" . "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" + "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //<--
                    "\t<mo> = MVA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum1prim_)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_evkprim_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr(S_evkprim_)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k2prim_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" . "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" + "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum102_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0,2</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0,2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation
                    "\t<mo>=</mo>\n" +
                    "\t<mo>"+generateStr(S_ksum102_)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv02_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_ekv02_)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double)mapValue.get("editText_Usrednosr"))+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k202_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>k,т2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->>
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_kt2)+" . "+generateStr(S_kk1_8)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_kt2)+" + "+generateStr(S_kk1_8)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //<--
                    "\t<mo> = "+generateStr(S_ksum18_)+" MVA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на пределно пропускателната мощност</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>екв</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>k,обр</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo>=</mo>\n" +
                    "\t<mo> "+generateStr(S_ksum18_)+" + "+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo>= "+generateStr(S_ekv8_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Определяне на тока на късо съединение в точка К2 при свръхпреходен режим:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>екв</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>ср,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_ekv8_)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k28_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>"+
                    "<p>"+
                    "\n" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>т.к.с., кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>K1</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_Iprim_K1"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I02_K1"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I8_K1"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>т. K2 паралелна работа</td>\n" +
                    "    <td>"+generateStr(I_k2prim)+"</td>\n" +
                    "    <td>"+generateStr(I_k202)+"</td>\n" +
                    "    <td>"+generateStr(I_k28)+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>т. K2 разделна работа</td>\n" +
                    "    <td>"+generateStr(I_k2prim_)+"</td>\n" +
                    "    <td>"+generateStr(I_k202_)+"</td>\n" +
                    "    <td>"+generateStr(I_k28_)+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+
                    "</body>";
        }
        else
            _html += "<body>\n" +
                    "<p>1.Short-circuit calculations</p>\n" +
                    "<p>We select Uaverage:</p>\n" +
                    "<p>For Urated, medium voltage ,kV = "+mapValue.get("editText_Unsrn")+"kV respectively Uaverage, medium voltage ,kV = "+mapValue.get("editText_Usrednosr")+"kV</p>\n" +
                    "<p>For Urated, high voltage ,kV = "+mapValue.get("editText_Unvn")+"kV respectively Uaverage, high voltage ,kV = "+mapValue.get("editText_Usrednovn")+"kV</p>\n" +
                    "<p>Equivalent circuit:  </p>\n" +
                    "<img src='"+ "file:///android_asset/1-1.png" +"' style='width:100px;height:180px;'/> "+
                    "<p>We select power transformer 1 with the following parameters: "+mapValue.get("editText_NameTrans1")+" Sn="+mapValue.get("editText_Sn_Trans1")+"MVA Un="+mapValue.get("editText_Un_Trans1")+" I0="+mapValue.get("editText_I0_Trans1")+",% Uk,%="+mapValue.get("editText_Uk_Trans1")+",% ΔPsteel="+mapValue.get("editText_Pst_Trans1")+"kW ΔPcu="+mapValue.get("editText_Pcu_Trans1")+"kW </p>\n" +
                    "<p>We select power transformer 2 with the following parameters: "+mapValue.get("editText_NameTrans2")+" Sn="+mapValue.get("editText_Sn_Trans2")+"MVA Un="+mapValue.get("editText_Un_Trans2")+" I0="+mapValue.get("editText_I0_Trans1")+",% Uk,%="+mapValue.get("editText_Uk_Trans2")+",% ΔPsteel="+mapValue.get("editText_Pst_Trans2")+"kW ΔPcu="+mapValue.get("editText_Pcu_Trans2")+"kW </p>\n" +
                    "<p>Parallel operation:</p>\n" +
                    "<p>Calculation of MVA at point K1:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>average</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    //calculation
                    "\t<mo>√3 . "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Iprim_K1"))+"</mo>\n" +
                    "\t<mo> = "+ generateStr(S_kk1_prim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>average</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    //calculation
                    "\t<mo> = </mo>\n" +
                    "\t<mo>√3 . "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_I02_K1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kk1_02)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,k1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>√3U</mn>\n" +
                    "\t<mi>average</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi></mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k1</mi>\n" +
                    "\t</msubsup>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>√3 "+generateStr((Double)mapValue.get("editText_Usrednovn"))+"</mo>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_I8_K1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kk1_8)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Calculation of MVA for power transformer 1:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<mn>s, transformer1</mn>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>u</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>,%</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>n</mi>\n" +
                    "\t</msub>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    ""+generateStr((Double)mapValue.get("editText_Uk_Trans1"))+""+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Sn_Trans1"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kt1)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Calculation of MVA for power transformer 2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<mn>s, transformer2</mn>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>u</mn>\n" +
                    "\t\t\t<mi>k</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>,%</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>n</mi>\n" +
                    "\t</msub>\n" +
                    //calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "    \t\t<mrow>100</mrow>\n" +
                    "\t\t<mrow>\n" +
                    ""+generateStr((Double)mapValue.get("editText_Uk_Trans2"))+""+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Sn_Trans2"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_kt2)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, transformerΣ</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, transformer1</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, transformer2</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = "+generateStr(S_kt1)+" + "+generateStr(S_kt2)+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ktsuma)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine</p>\n" +
                    "<p align='center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>.</mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo>+</mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo>= "+generateStr(S_ksuma1prim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Equivalent circuit:</p>\n" +
                    "<img src='"+ "file:///android_asset/1-2.png" +"' style='width:100px;height:180px;'/>"+
                    "<img src='"+ "file:///android_asset/1-3.png" +"' style='width:150px;height:100px;'/>"+
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksuma1prim)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekvprim)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>√3.</mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>average,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ekvprim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo> √3 . "+generateStr((Double)mapValue.get("editText_Usrednosr"))+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k2prim)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>sm transformerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum102)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum102)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv02)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>average,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ekv02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>√3 . "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k202)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>s, Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transfomerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transfomerΣ</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" . "+generateStr(S_kk1_8)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_ktsuma)+" + "+generateStr(S_kk1_8)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum18)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum18)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv8)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>average,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr(S_ekv8)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k28)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Independent operation:</p>\n" +
                    "<p>Equivalent circuit:</p>\n" +
                    "<img src='"+ "file:///android_asset/1-4.png" +"' style='width:150px;height:100px;'/>"+
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" . "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" + "+generateStr(S_kk1_prim)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //<--
                    "\t<mo> = MVA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mo>"+generateStr(S_ksum1prim_)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_evkprim_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>\"</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>\"</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>average,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo>"+generateStr(S_evkprim_)+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double) mapValue.get("editText_Usrednosr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k2prim_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" . "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "<mo>"+generateStr(S_kt2)+" + "+generateStr(S_kk1_02)+"</mo>"+
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(S_ksum102_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0,2</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0,2</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation
                    "\t<mo>=</mo>\n" +
                    "\t<mo>"+generateStr(S_ksum102_)+"</mo>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<mo>"+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo> = "+generateStr(S_ekv02_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>0.2</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>0.2</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>average,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "<mo>=</mo>"+
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_ekv02_)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double)mapValue.get("editText_Usrednosr"))+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k202_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transformer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> . </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>S</mn>\n" +
                    "\t\t\t<mi>s, transfomer2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t\t<mo> + </mo>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>k,k1</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->>
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_kt2)+" . "+generateStr(S_kk1_8)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_kt2)+" + "+generateStr(S_kk1_8)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //<--
                    "\t<mo> = "+generateStr(S_ksum18_)+" MVA</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine MVA</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>equiv</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>=</mo>\n" +
                    "\t<mi>S</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k,Σ1</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> + </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>S</mn>\n" +
                    "\t<mi>s, reverse</mi>\n" +
                    "\t</msub>\n" +
                    //Calculation -->
                    "\t<mo>=</mo>\n" +
                    "\t<mo> "+generateStr(S_ksum18_)+" + "+generateStr((Double) mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t<mo>= "+generateStr(S_ekv8_)+" MVA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>We determine the subtransient short-circuit current at point K2:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>∞</mn>\n" +
                    "\t<mi>k2</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mi>S</mi>\n" +
                    "\t\t\t<msubsup class=\"supsub\">\n" +
                    "\t\t\t<mi> </mi>\n" +
                    "\t\t\t<mn>∞</mn>\n" +
                    "\t\t\t<mi>equiv</mi>\n" +
                    "\t\t\t</msubsup>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. </mo>\n" +
                    "\t\t\t<msub>\n" +
                    "\t\t\t<mn>U</mn>\n" +
                    "\t\t\t<mi>reverse,k2</mi>\n" +
                    "\t\t\t</msub>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    //Calculation -->
                    "\t<mo> = </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> "+generateStr(S_ekv8_)+" </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<mo> √3. "+generateStr((Double)mapValue.get("editText_Skobr"))+"</mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> = "+generateStr(I_k28_)+" kA</mo>\n" +
                    //<--
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>"+
                    "<p>"+
                    "\n" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>short-circuit current, кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>K1</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_Iprim_K1"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I02_K1"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I8_K1"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>point K2 parallel operation</td>\n" +
                    "    <td>"+generateStr(I_k2prim)+"</td>\n" +
                    "    <td>"+generateStr(I_k202)+"</td>\n" +
                    "    <td>"+generateStr(I_k28)+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>point K2 independent operation</td>\n" +
                    "    <td>"+generateStr(I_k2prim_)+"</td>\n" +
                    "    <td>"+generateStr(I_k202_)+"</td>\n" +
                    "    <td>"+generateStr(I_k28_)+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+
                    "</body>";
        return _html;
    }

    @Override
    public File getFile() {
        return file;
    }
}
