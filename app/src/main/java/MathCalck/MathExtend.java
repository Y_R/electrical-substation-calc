package MathCalck;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import fragments.ImagesObj;

/**
 * Created by YR on 11/14/2016.
 */

public class MathExtend implements Math {

    private int lang;

    @Override
    public void library(Context context) throws IOException {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
    }

    @Override
    public void save(File file, String createHTML) {
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try{
            fos.write(createHTML.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void language(int _lang) {
        lang = _lang;
    }

    @Override
    public int getLanguage() {
        return lang;
    }

    @Override
    public String createHeaderHTMLI(String _html){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";

        _html = "<!DOCTYPE html>\n" +
                "<html lang=\"en\" xmlns:m=\"http://www.w3.org/1998/Math/MathML\">\n" +
                "<style>\n" +
                "sup {\n" +
                "\tbackground-color: lightblue;\n" +
                "\tposition: relative;\n" +
                "}\n" +
                "</style>\n" +
                "<style>\n" +
                ".supsub {\n" +
                "    display: inline-block;\n" +
                "}\n" +
                "\n" +
                ".supsub mn,\n" +
                ".supsub mi {\n" +
                "    position: relative;\n" +
                "    display: block;\n" +
                "    line-height: 1.2;\n" +
                "}\n" +
                "\n" +
                ".supsub mi {\n" +
                "    top: .3em;\n" +
                "}\n" +
                "table {\n" +
                "    border-collapse: collapse;\n" +
                "}\n" +
                "\n" +
                "table, td, th {\n" +
                "    border: 1px solid black;\n" +
                "}"+
                "</style>"+
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t\n" +
                "\t<link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=UnifrakturMaguntia\">\n" +
                //"\t<link rel=\"stylesheet\" href=\""+path+"/jqmath-0.4.3.css\">\n" +
                "\t<link rel=\"stylesheet\" href='"+"file:///android_asset/mathscribe/jqmath-0.4.3.css"+"'>\n" +
                "\t\n" +
                //"\t<script src=\""+path+"/jquery-1.4.3.min.js\"></script>\n" +
                "\t<script src='"+"file:///android_asset/mathscribe/jquery-1.4.3.min.js"+"'></script>\n" +
                //"\t<script src=\""+path+"/jqmath-etc-0.4.5.min.js\" charset=\"utf-8\"></script>\n" +
                "\t<script src='"+"file:///android_asset/mathscribe/jqmath-etc-0.4.5.min.js"+"' charset=\"utf-8\"></script>\n" +
                "\t<title>@@@ Put your title here</title>\n" +
                "</head>\n";

        return _html;
    }

    @Override
    public String createBodyHTML(String _html) throws IOException {
        return null;
    }


    @Override
    public String closeHTML(String _html){
        _html += "</html>\n";
        return _html;
    }

    @Override
    public String createHTML() throws IOException {
        String _html = null;
        _html = this.createHeaderHTMLI(_html);
        _html = this.createBodyHTML(_html);
        _html = this.closeHTML(_html);

        return _html;
    }

    @Override
    public File getFile() {
        return null;
    }

    @Override
    public String generateStr(Double _number) {
        return String.format(Locale.GERMAN,"%.2f", _number);
    }

    @Override
    public void setCheckedImages(ArrayList<ImagesObj> checkedImages) {

    }

    @Override
    public String generateStr(Double _number, int numSign) {
        return String.format(Locale.GERMAN,"%."+numSign+"f", _number);
    }

}
