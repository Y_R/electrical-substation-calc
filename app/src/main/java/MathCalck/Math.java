package MathCalck;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import fragments.ImagesObj;

/**
 * Created by YR on 11/14/2016.
 */

public interface Math {

    void library(Context context) throws IOException;
    void save(File file, String createHTML);
    String createHeaderHTMLI(String _html);
    String createBodyHTML(String _html) throws IOException;
    String closeHTML(String _html);
    String createHTML() throws IOException;
    File getFile();
    String generateStr(Double _number, int numSign);
    String generateStr(Double _number);
    void setCheckedImages(ArrayList<ImagesObj> checkedImages);
    void language(int _lang);
    int getLanguage();
}
