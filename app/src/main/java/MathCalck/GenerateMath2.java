package MathCalck;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.*;
import java.lang.Math;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by YR on 11/14/2016.
 */

public class GenerateMath2 extends MathExtend{

    private Context context;
    private File file;
    private Map<String,Object> mapValue = new HashMap<>();

    public GenerateMath2(Context _context, Map<String, Object> _mapValue){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
        context = _context;
        mapValue = _mapValue;
        File dir = new  File(path);
        dir.mkdir();
        file  = new File(path + "/file.html");
        if(file.exists())
            file.delete();
        else{
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            this.library(_context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String createBodyHTML(String _html){
        Double Ip = 1 * (Double)mapValue.get("editText_Iprim_K2");
        Double Ta = (Double)mapValue.get("editText_xr") / (2 * 3.14 * 50);

        Double natural_e = Math.pow(2.71828,-((Double)mapValue.get("editText_vremeZashtitaMinimalno")/Ta));
        Double ia = natural_e * Ip;
        Double I_uslovie1 = Math.sqrt(2) * (Double)mapValue.get("editText_Inomizk") * (1 + ((Double)mapValue.get("editText_beta")/100));
        Double I_uslovie2 = Math.sqrt(2) * Ip + ia;
        Double Ky = 1 + natural_e;
        Double Iy = Math.sqrt(2) * (Double)mapValue.get("editText_Iprim_K2") * Ky;
        Double t = (Double)mapValue.get("editText_vremeZashtitaIzkliuchvane") + (Double)mapValue.get("editText_tizkl")/1000;
        Double Bk = (Math.pow((Double)mapValue.get("editText_I8_K2"),2) * t) + (Math.pow((Double)mapValue.get("editText_Iprim_K2"),2) * Ta);


        if(getLanguage() == 0){
            _html +="<body>\n" +
                    "<p>1.Изчисляване на токовете на късо съединение</p>\n" +
                    "<p>"+
                    "\n" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>т.к.с., кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>Ток на късо съединение в мястото на присъединяване</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I02_K2"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I8_K2"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+

                    "<p>2.Избор на прекъсвачи и разединители(комутационна апаратура)</p>\n" +
                    "<p>Избираме прекъсвач "+mapValue.get("editText_NamePrekasvach")+" </p>\n" +
                    "<p>Условие за избор по ниво на изолация: $U_n≥U_р $"+
                    "$$"+generateStr((Double)mapValue.get("editText_Un"))+"kV≥"+generateStr((Double)mapValue.get("editText_Un"))+"kV$$</p>\n" +
                    "<p>$I_n≥$Работен ток, A:$</p>\n" +
                    "<p>$$"+generateStr((Double)mapValue.get("editText_In"))+" A ≥ "+generateStr((Double)mapValue.get("editText_A"))+" A $$</p>\n" +
                    "<p>Прекъсвачът има следните параметри:</p>\n" +
                    "<p>1.Номинално напрежение $U_n=\\text "+generateStr((Double)mapValue.get("editText_Un"))+"kV$</p>\n" +
                    "<p>2.Номинален ток $I_n=\\text "+generateStr((Double)mapValue.get("editText_In"))+"A$</p>\n" +
                    "<p>3.Номинален ток на изключване $I_\\text \"ном.изкл.\"=\\text "+generateStr((Double)mapValue.get("editText_Inomizk"))+" kА$</p>\n" +
                    "<p>4.Ток на термична устойчивост $I_t=\\text \" "+generateStr((Double)mapValue.get("editText_Itermust"))+" kA/s\"$</p>\n" +
                    "<p>5.Ток на включване $I_\\text \"вкл.\"=\\text "+generateStr((Double)mapValue.get("editText_Ivkl"))+" kA$</p>\n" +
                    "<p>6.Пълно време за изключване на прекъсвача $t_\\text \"изкл.\"=\\text "+generateStr((Double)mapValue.get("editText_tizkl"))+" ms$</p>\n" +
                    "<p>7.Време за горене на дъгата $t_\\text \"дъга\"=\\text "+generateStr((Double)mapValue.get("editText_tduga"))+" ms$</p>\n" +
                    "<p>8.Постоянна съставка на тока на изключване $β= "+generateStr((Double)mapValue.get("editText_beta"))+" %$</p>\n" +
                    "<p>Проверка по изключвателна способност:</p>\n" +
                    "<p>По периодична съставка на тока на късо съединение </p>\n" +
                    "$$I_П=γ.I\"=\\text 1."+generateStr((Double)mapValue.get("editText_Iprim_K2"))+" = "+generateStr((Double)Ip)+" kA$$\n" +
                    "<p>Условие за избор $I_\\text \"ном.изкл.\"≥I_П$</p>" +
                    "<p>" +
                    "$$"+generateStr((Double)mapValue.get("editText_Inomizk"))+"kA≥ "+generateStr((Double)Ip)+"kA$$" +
                    "</p>\n" +
                    "<p>По пълен ток на изключване:</p>\n" +
                    "<p>t - минимално време за изключване на релейната защита t="+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"s</p>\n" +
                    "<p>$T_a=x/{ω.r}= $ " +
                    "${"+generateStr((Double)mapValue.get("editText_xr"))+"}/{2.π.50}= $" +
                    "$"+generateStr((Double)Ta,3)+" s$ " +
                    "- времеконстанта на затихване на апериодичната съставка на тока на късо съединение</p>\n" +
                    "<p>Определяне на апериодичната съставка на тока на късо съединение </p>\n" +
                    "<p>$$i_a=I_П.e^{-t/{T_a}}= " +
                    generateStr((Double)Ip)+".e^{-{"+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"}/{"+generateStr((Double)Ta,3)+"}}= "+generateStr((Double)ia)+" kA$$</p>\n" +
                    "<p>Проверка по изключвателна способност: </p>\n" +
                    "$$√2 .I_\\text \"ном.изкл.\".(1+β_\\text \"ном.\")≥√2 .I_П+i_a$$\n" +
                    "$$√2 ."+generateStr((Double)mapValue.get("editText_Inomizk"))+".(1+{"+generateStr((Double)mapValue.get("editText_beta"))+"}/{100})≥√2 ."+generateStr((Double)Ip)+"+"+generateStr((Double)ia)+"$$\n" +
                    "<p>Условието</p>\n" +
                    "$$"+generateStr((Double)I_uslovie1)+"≥"+generateStr((Double)I_uslovie2)+"$$\n" +
                    "<p>Проверка по ток на включване и ток на динамична устойчивост:</p>\n" +
                    "<p>Изчисляване на ударния коефициент</p>\n" +
                    "$$k_y=1+e^{-t/{T_a}}=1+e^{-{"+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"}/{"+generateStr((Double)Ta,3)+"}} = "+generateStr((Double)Ky)+"$$\n" +
                    "<p>Ударният ток </p>\n" +
                    "$$i_y=√2 .I\".k_y= " +
                    "√2 ."+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"."+generateStr((Double)Ky)+"=" +
                    ""+generateStr((Double)Iy)+" kA$$\n" +
                    "<p>Условието </p>\n" +
                    "$$I_\\text \"вкл\"≥i_y$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Ivkl"))+" kA≥ "+generateStr((Double)Iy)+" kA$$\n" +
                    "<p>Проверка по термична устойчивост</p>\n" +
                    "<p>Изчисляване на пълното време за действието на тока на късо съединение: </p>\n" +
                    "$$t=t_\\text \"п.п.\"+t_\\text \"рз\"=(t_\\text \"с.п.\"+t_\\text \"дъга\")+t_\\text \"пз\"="+generateStr((Double)mapValue.get("editText_tizkl")/1000,3)+" + "+generateStr((Double)mapValue.get("editText_vremeZashtitaIzkliuchvane"))+" = "+generateStr((Double)mapValue.get("editText_tizkl")/1000 + (Double)mapValue.get("editText_vremeZashtitaIzkliuchvane"),3)+"s$$\n" +
                    "<p>Определяне на топлинния импулс </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>β</mn>\n" +
                    "\t\t\t<mi>′′2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> + 3 </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>4</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>∞</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . t + </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>I</mn>\n" +
                    "\t<mi>\"2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>T</mn>\n" +
                    "\t<mi>a</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>1</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> + 3 </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>4</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_I8_K2"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . "+generateStr((Double)t,3)+" + </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)Ta,3)+"</mo>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn> "+generateStr((Double)Bk,3)+" kA</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo>s</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>Условието </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>доп</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +

                    "</math>\n" +
                    "<br>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_Itermust"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_ttermust"))+"</mo>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<mo>"+generateStr((Double)Bk,3)+"</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust"),2) * (Double)mapValue.get("editText_ttermust")) +" kA^2.s≥ "+generateStr((Double)Bk,3)+" kA^2.s$\n" +
                    "<p>Избираме разединител с параметри Тип </p>\n" +
                    mapValue.get("editText_NameRazedinitel")+", $U_n$= "+generateStr((Double)mapValue.get("editText_Unom"))+" kV, $I_n$="+generateStr((Double)mapValue.get("editText_Inom"))+" A, $I_din$="+generateStr((Double)mapValue.get("editText_Idin"))+" kA, $I_t$="+generateStr((Double)mapValue.get("editText_Itermust_raz"))+"kA / "+generateStr((Double)mapValue.get("editText_tterm_raz"))+" s\n" +
                    "<p>Условието </p>\n" +
                    "$$I_n≥I_\\text \"max.раб.\"$$\n" +
                    "$$"+generateStr((Double)mapValue.get("editText_Inom"))+" А≥ "+generateStr((Double)mapValue.get("editText_A"))+" А$$\n" +
                    "<p>Условието</p>\n" +
                    "$$U_\\text \"п.раз.\"≥U_n$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Unom"))+" kV ≥ "+generateStr((Double)mapValue.get("editText_Unsrn"))+" kV$$\n" +
                    "<p>Проверка по термична устойчивост: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>доп</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Itermust_raz"))+" kA^2 . "+generateStr((Double)mapValue.get("editText_ttermust"))+"≥ "+generateStr((Double)Bk,3)+" kA^2.s$$\n" +
                    "$$ "+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_raz"),2) * (Double)mapValue.get("editText_ttermust"))+" kA^2.s≥ "+generateStr((Double)Bk,3)+" kA^2.s$$\n" +
                    "<p>"+
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>Условие за избор</th>\n" +
                    "    <th>Разчетни данни</th>\n" +
                    "    <th>Номинални данни прекъсвач</th>\n" +
                    "    <th>Номинални данни разединител</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$U_n ≥ U_p$</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Unsrn"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Un"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Unom"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_n ≥ I_{max. раб.}$</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_A"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_In"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Inom"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_{ном.изк.} ≥ I_п$</td>\n" +
                    "    <td>"+generateStr((Double) Ip)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Inomizk"))+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$√2.I_{ном.изкл.}.(1+β_{ном.}) ≥ 2 – √2.I_{П}+i_a$</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie2)+"</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie1)+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_{вкл} ≥ i_y$</td>\n" +
                    "    <td>"+generateStr((Double) Iy)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Ivkl"))+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$i_{din} ≥ i_y$</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie2)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Ivkl"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Idin"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>доп</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +

                    "</math>\n" +
                    "</td>\n" +
                    "    <td>"+generateStr((Double) Bk,3)+"</td>\n" +
                    "    <td>"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust"),2) * (Double)mapValue.get("editText_ttermust"))+"</td>\n" +
                    "    <td>"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_raz"),2) * (Double)mapValue.get("editText_ttermust"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+

                    "</body>";
        }
        else{
            _html += "<body>\n" +
                    "<p>1.Short-circuit calculations</p>\n" +
                    "<p>"+
                    "\n" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>short-circuit current, кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>Short-circuit current at the point of connection</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I02_K2"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I8_K2"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+

                    "<p>Circuit-breakers and switch-disconnectors selection</p>\n" +
                    "<p>Circuit-breaker selection for "+mapValue.get("editText_NamePrekasvach")+" </p>\n" +
                    "<p>Insulation level condition: $U_n≥U_\\text \"operating\" $" +
                    "$$"+generateStr((Double)mapValue.get("editText_Un"))+"kV≥"+generateStr((Double)mapValue.get("editText_Un"))+"kV$$</p>\n" +
                    "<p>$I_n≥$Rated current, A:$</p>\n" +
                    "<p>$$"+generateStr((Double)mapValue.get("editText_In"))+" A ≥ "+generateStr((Double)mapValue.get("editText_A"))+" A $$</p>\n" +
                    "<p>The chosen circuit-breaker is with the following parameters:</p>\n" +
                    "<p>1.Rated voltage $U_n=\\text "+generateStr((Double)mapValue.get("editText_Un"))+"kV$</p>\n" +
                    "<p>2.Rated current $I_n=\\text "+generateStr((Double)mapValue.get("editText_In"))+"A$</p>\n" +
                    "<p>3.Rated breaking current $I_\\text \"breaking\"=\\text "+generateStr((Double)mapValue.get("editText_Inomizk"))+" kА$</p>\n" +
                    "<p>4.Rated short-time withstand current $I_t=\\text \" "+generateStr((Double)mapValue.get("editText_Itermust"))+" kA/s\"$</p>\n" +
                    "<p>5.Making capacity $I_\\text \"making\"=\\text "+generateStr((Double)mapValue.get("editText_Ivkl"))+" kA$</p>\n" +
                    "<p>6.Full circuit-breaker breaking time $t_\\text \"breaking\"=\\text "+generateStr((Double)mapValue.get("editText_tizkl"))+" ms$</p>\n" +
                    "<p>7.Arcing time $t_\\text \"arc\"=\\text "+generateStr((Double)mapValue.get("editText_tduga"))+" ms$</p>\n" +
                    "<p>8.Breaking current DC component $β= "+generateStr((Double)mapValue.get("editText_beta"))+" %$</p>\n" +
                    "<p>Breaking capacity condition:</p>\n" +
                    "<p>Short-circuit current periodic component </p>\n" +
                    "$$I_\\text \"periodic\"=γ.I\"=\\text 1."+generateStr((Double)mapValue.get("editText_Iprim_K2"))+" = "+generateStr((Double)Ip)+" kA$$\n" +
                    "<p>Selection condition $I_\\text \"breaking\"≥I_\\text \"periodic\"$" +
                    "$$"+generateStr((Double)mapValue.get("editText_Inomizk"))+"kA≥ "+generateStr((Double)Ip)+"kA$$</p>\n" +
                    "<p>Selection by full breaking current</p>\n" +
                    "<p>t - minimal time for trip signal from relay protection t="+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"s</p>\n" +
                    "<p>$T_a=x/{ω.r}= $ " +
                    "${"+generateStr((Double)mapValue.get("editText_xr"))+"}/{2.π.50}= $" +
                    "$"+generateStr((Double)Ta,3)+" s$ " +
                    "- time constant of exponential decay of the aperiodic short-circuit component</p>\n" +
                    "<p>We determine the aperiodic short-circuit component</p>\n" +
                    "<p>$$i_a=I_\\text \"periodic\".e^{-t/{T_a}}= " +
                    ""+generateStr((Double)Ip)+".e^{-{"+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"}/{"+generateStr((Double)Ta,3)+"}}= "+generateStr((Double)ia)+" kA$$</p>\n" +
                    "<p>Breaking capacity condition:</p>\n" +
                    "$$√2 .I_\\text \"breaking\".(1+β_\\text \"rated\")≥√2 .I_\\text \"periodic\"+i_a$$\n" +
                    "$$√2 ."+generateStr((Double)mapValue.get("editText_Inomizk"))+".(1+{"+generateStr((Double)mapValue.get("editText_beta"))+"}/{100})≥√2 ."+generateStr((Double)Ip)+"+"+generateStr((Double)ia)+"$$\n" +
                    "<p>The condition</p>\n" +
                    "$$"+generateStr((Double)I_uslovie1)+"≥"+generateStr((Double)I_uslovie2)+"$$\n" +
                    "<p>Making capacity and short-time withstand current condition:</p>\n" +
                    "<p>Pick-up factor calculation</p>\n" +
                    "$$k_\\text \"pick-up\"=1+e^{-t/{T_a}}=1+e^{-{"+generateStr((Double)mapValue.get("editText_vremeZashtitaMinimalno"),3)+"}/{"+generateStr((Double)Ta,3)+"}} = "+generateStr((Double)Ky)+"$$\n" +
                    "<p>Pick-up factor calculation </p>\n" +
                    "$$i_\\text \"pick-up\"=√2 .I\".k_\\text \"pick-up\"= " +
                    "√2 ."+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"."+generateStr((Double)Ky)+"= " +
                    "$"+generateStr((Double)Iy)+" kA$$\n" +
                    "<p>The condition </p>\n" +
                    "$$I_\\text \"making\"≥i_\\text \"pick-up\"\n" +
                    " "+generateStr((Double)mapValue.get("editText_Ivkl"))+" kA≥ "+generateStr((Double)Iy)+" kA$$\n" +
                    "<p>Rated short-time withstand current condition</p>\n" +
                    "<p>Calculating the total time of operation of the short-circuit current:</p>\n" +
                    "$$t=t_\\text \"s.b.\"+t_\\text \"s.d.\"=(t_\\text \"s.b.\"+t_\\text \"arc\")+t_\\text \"relay\"="+generateStr((Double)mapValue.get("editText_tizkl")/1000,3)+" + "+generateStr((Double)mapValue.get("editText_vremeZashtitaIzkliuchvane"))+" = "+generateStr((Double)mapValue.get("editText_tizkl")/1000 + (Double)mapValue.get("editText_vremeZashtitaIzkliuchvane"),3)+"s$$\n" +
                    "<p>We determine the thermal pulse </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>β</mn>\n" +
                    "\t\t\t<mi>′′2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> + 3 </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>4</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>∞</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . t + </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>I</mn>\n" +
                    "\t<mi>\"2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>T</mn>\n" +
                    "\t<mi>a</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<mfrac>\n" +
                    "\t\t<mrow>\n" +
                    "\t\t\t<msup>\n" +
                    "\t\t\t<mn>1</mn>\n" +
                    "\t\t\t<mi>2</mi>\n" +
                    "\t\t\t</msup>\n" +
                    "\t\t\t<mo> + 3 </mo>\n" +
                    "\t\t</mrow>\n" +
                    "\t\t<mrow>4</mrow>\n" +
                    "\t</mfrac>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_I8_K2"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . "+generateStr((Double)t,3)+" + </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_Iprim_K2"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)Ta,3)+"</mo>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≈ </mo>\n" +
                    "\t<msup>\n" +
                    "\t<mn> "+generateStr((Double)Bk,3)+" kA</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo>s</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "<p>The condition </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>rated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +

                    "</math>\n" +
                    "<br>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<msup>\n" +
                    "\t<mn>"+generateStr((Double)mapValue.get("editText_Itermust"))+"</mn>\n" +
                    "\t<mi>2</mi>\n" +
                    "\t</msup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<mo>"+generateStr((Double)mapValue.get("editText_ttermust"))+"</mo>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<mo>"+generateStr((Double)Bk,3)+"</mo>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust"),2) * (Double)mapValue.get("editText_ttermust")) +" kA^2.s≥ "+generateStr((Double)Bk,3)+" kA^2.s$\n" +
                    "<p>The selected switch-disconnector is with the following parameters Type</p>\n" +
                    mapValue.get("editText_NameRazedinitel")+",$U_n$= "+generateStr((Double)mapValue.get("editText_Unom"))+" kV, $I_n$="+generateStr((Double)mapValue.get("editText_Inom"))+" A,$I_din$="+generateStr((Double)mapValue.get("editText_Idin"))+" kA,$I_t$="+generateStr((Double)mapValue.get("editText_Itermust_raz"))+"kA / "+generateStr((Double)mapValue.get("editText_tterm_raz"))+" s\n" +
                    "<p>The condition </p>\n" +
                    "$$I_n≥I_\\text \"max.operational\"$$\n" +
                    "$$"+generateStr((Double)mapValue.get("editText_Inom"))+" А≥ "+generateStr((Double)mapValue.get("editText_A"))+" А$$\n" +
                    "<p>The condition</p>\n" +
                    "$$U_\\text \"rated\"≥U_n$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Unom"))+" kV ≥ "+generateStr((Double)mapValue.get("editText_Unsrn"))+" kV$$\n" +
                    "<p>Rated short-time withstand current condition: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>rated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Itermust_raz"))+" kA^2 . "+generateStr((Double)mapValue.get("editText_ttermust"))+"≥ "+generateStr((Double)Bk,3)+" kA^2.s$$\n" +
                    "$$ "+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_raz"),2) * (Double)mapValue.get("editText_ttermust"))+" kA^2.s≥ "+generateStr((Double)Bk,3)+" kA^2.s$$\n" +
                    "<p>"+
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>Selection condition</th>\n" +
                    "    <th>Rated data</th>\n" +
                    "    <th>Rated data circuit-breaker</th>\n" +
                    "    <th>Rated data switch-disconnector</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$U_n ≥ U_p$</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Unsrn"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Un"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Unom"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_n ≥ I_{max. раб.}$</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_A"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_In"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Inom"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_{ном.изк.} ≥ I_п$</td>\n" +
                    "    <td>"+generateStr((Double) Ip)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Inomizk"))+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$√2.I_{ном.изкл.}.(1+β_{ном.}) ≥ 2 – √2.I_{П}+i_a$</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie2)+"</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie1)+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$I_{вкл} ≥ i_y$</td>\n" +
                    "    <td>"+generateStr((Double) Iy)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Ivkl"))+"</td>\n" +
                    "    <td> - </td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>$i_{din} ≥ i_y$</td>\n" +
                    "    <td>"+generateStr((Double) I_uslovie2)+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Ivkl"))+"</td>\n" +
                    "    <td>"+generateStr((Double) mapValue.get("editText_Idin"))+"</td>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo> . </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>rated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</td>\n" +
                    "    <td>"+generateStr((Double) Bk,3)+"</td>\n" +
                    "    <td>"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust"),2) * (Double)mapValue.get("editText_ttermust"))+"</td>\n" +
                    "    <td>"+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_raz"),2) * (Double)mapValue.get("editText_ttermust"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>"+
                    "</body>";
        }

        return _html;
    }

    @Override
    public File getFile() {
        return file;
    }

}
