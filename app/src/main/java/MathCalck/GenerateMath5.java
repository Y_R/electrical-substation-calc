package MathCalck;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fragments.ImagesObj;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class GenerateMath5 extends MathExtend {

    private Context context;
    private File file;
    private Map<String,Object> mapValue = new HashMap<>();

    private ArrayList<ImagesObj> checkedImages;


    public GenerateMath5(Context _context, Map<String, Object> _mapValue){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Calculator";
        context = _context;
        mapValue = _mapValue;
        File dir = new  File(path);
        dir.mkdir();
        file  = new File(path + "/file.html");
        if(file.exists())
            file.delete();
        else{
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            this.library(_context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setCheckedImages(ArrayList<ImagesObj> checkedImages) {
        this.checkedImages = new ArrayList<>(checkedImages);
    }

    @Override
    public String createBodyHTML(String _html){

        Double Ta = (Double)mapValue.get("editText_xr_5") / (2 * 3.14 * 50);
        Double t = (Double)mapValue.get("editText_MaksVremeIzklRel_5") + (Double)mapValue.get("editText_MinVremeIzklRel_5")/1000;
        Double Bk = (Math.pow((Double)mapValue.get("editText_I8_K2_5"),2) * t) + (Math.pow((Double)mapValue.get("editText_Iprim_K2_5"),2) * Ta);

        String images="";
        for(ImagesObj imagesObj : checkedImages){
            images += "<img src='"+ "file:///android_res/drawable/"+imagesObj.getPosition()+"" +"' style='width:100px;height:100px;'/> ";
        }



        if(getLanguage() == 0) {
            _html += "<body>\n" +
                    "<p>1.Изчисляване на токовете на късо съединение</p>\n" +
                    "<p>" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>т.к.с., кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>Ток на късо съединение в мястото на присъединяване</td>\n" +
                    "    <td>" + generateStr((Double) mapValue.get("editText_Iprim_K2_5")) + "</td>\n" +
                    "    <td>" + generateStr((Double) mapValue.get("editText_I02_K2_5")) + "</td>\n" +
                    "    <td>" + generateStr((Double) mapValue.get("editText_I8_K2_5")) + "</td>\n" +
                    "  </tr>\n" +
                    "</table>\n" +
                    "</p>\n" +
                    "<p>Избор на КРУ</p>\n" +
                    "<p>Избор на елементи в КРУ:</p>\n" +
                    "<p>" + images + "</p>\n" +
                    "<p>Избираме КРУ с параметри Тип </p>\n" +
                    "<p>$$" + mapValue.get("editText_name_5") + ", U_n= " + generateStr((Double) mapValue.get("editText_Unom_5")) + " kV,I_n= " + generateStr((Double) mapValue.get("editText_Inom_5")) + " A, I_d_i_n= " + generateStr((Double) mapValue.get("editText_Idin_5")) + " kA,I_t= " + generateStr((Double) mapValue.get("editText_Itermust_5")) + " kA/" + generateStr((Double) mapValue.get("editText_ttermust_5")) + "s$$</p>\n" +
                    "<p>Условието </p>\n" +
                    "$$I_n≥I_\\text \"max.раб.\"$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Inom_5")) + " A ≥ " + generateStr((Double) mapValue.get("editText_A_5")) + " A$$\n" +
                    "<p>Условието </p>\n" +
                    "$$U_\\text \"n.раз.\"≥U_n$$\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Unom_5")) + " kV≥ " + generateStr((Double) mapValue.get("editText_MaksU_5")) + " kV$$\n" +
                    "<p>Проверка по термична устойчивост: </p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>доп</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ " + generateStr((Double) mapValue.get("editText_Itermust_5")) + "^2 kA." + generateStr((Double) mapValue.get("editText_ttermust_5")) + "s ≥ " + generateStr((Double) Bk, 3) + " kA^2.s $$\n" +
                    "$$ " + generateStr(Math.pow((Double) mapValue.get("editText_Itermust_5"), 2) * (Double) mapValue.get("editText_ttermust_5")) + " kA^2.s ≥ " + generateStr((Double) Bk, 3) + " kA^2.s $$" +
                    "</body>";
        }
        else{
            _html += "<body>\n" +
                    "<p>1.Short-circuit calculations</p>\n" +
                    "<p>" +
                    "<table>\n" +
                    "  <tr>\n" +
                    "    <th>short-circuit current, кА</th>\n" +
                    "    <th>I''</th>\n" +
                    "    <th>I02</th>\n" +
                    "    <th>I∞</th>\n" +
                    "  </tr>\n" +
                    "  <tr>\n" +
                    "    <td>ТShort-circuit current at the point of connection</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_Iprim_K2_5"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I02_K2_5"))+"</td>\n" +
                    "    <td>"+generateStr((Double)mapValue.get("editText_I8_K2_5"))+"</td>\n" +
                    "  </tr>\n" +
                    "</table>\n"+
                    "</p>\n" +
                    "<p>Switchgear selection</p>\n" +
                    "<p>Switchgear components selection:</p>\n" +
                    "<p>"+images+"</p>\n" +
                    "<p>We choose switchgear with the following parameters Type </p>\n" +
                    "<p>$$"+mapValue.get("editText_name_5")+", U_n= "+generateStr((Double)mapValue.get("editText_Unom_5"))+" kV,I_n= "+generateStr((Double)mapValue.get("editText_Inom_5"))+" A, I_d_i_n= "+generateStr((Double)mapValue.get("editText_Idin_5"))+" kA,I_t= "+generateStr((Double)mapValue.get("editText_Itermust_5"))+" kA/"+generateStr((Double)mapValue.get("editText_ttermust_5"))+"s$$</p>\n" +
                    "<p>The condition</p>\n" +
                    "$$I_n≥I_\\text \"max.operational\"$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Inom_5"))+" A ≥ "+generateStr((Double)mapValue.get("editText_A_5"))+" A$$\n" +
                    "<p>The condition</p>\n" +
                    "$$U_\\text \"rated\"≥U_n$$\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Unom_5"))+" kV≥ "+generateStr((Double)mapValue.get("editText_MaksU_5"))+" kV$$\n" +
                    "<p>Rated short-time withstand current condition:</p>\n" +
                    "<p align = 'center'>\n" +
                    "<math>\n" +
                    "<mrow>\n" +
                    "\t<mi>I</mi>\n" +
                    "\t<msubsup class=\"supsub\">\n" +
                    "\t<mi> </mi>\n" +
                    "\t<mn>2</mn>\n" +
                    "\t<mi>t</mi>\n" +
                    "\t</msubsup>\n" +
                    "\t<mo>.</mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>t</mn>\n" +
                    "\t<mi>rated</mi>\n" +
                    "\t</msub>\n" +
                    "\t<mo> ≥ </mo>\n" +
                    "\t<msub>\n" +
                    "\t<mn>B</mn>\n" +
                    "\t<mi>k</mi>\n" +
                    "\t</msub>\n" +
                    "</mrow>\n" +
                    "</math>\n" +
                    "</p>\n" +
                    "$$ "+generateStr((Double)mapValue.get("editText_Itermust_5"))+"^2 kA."+generateStr((Double)mapValue.get("editText_ttermust_5"))+"s ≥ "+generateStr((Double)Bk,3)+" kA^2.s $$\n" +
                    "$$ "+generateStr(Math.pow((Double)mapValue.get("editText_Itermust_5"),2) * (Double)mapValue.get("editText_ttermust_5"))+" kA^2.s ≥ "+generateStr((Double)Bk,3)+" kA^2.s $$"+
                    "</body>";
        }
        return _html;
    }

    @Override
    public File getFile() {
        return file;
    }

}
