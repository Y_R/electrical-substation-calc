package application.android.yr.calculator_tu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import MathCalck.GenerateMath;
import MathCalck.GenerateMath2;
import MathCalck.GenerateMath3;
import MathCalck.GenerateMath4;
import MathCalck.GenerateMath5;
import MathCalck.MathExtend;
import fragments.ImagesObj;

public class WebViewActivity extends AppCompatActivity {


    private WebView webView;
    private ProgressBar progressBar;
    ArrayList<ImagesObj> checkedImages = new ArrayList<>();
    int i;

    String path;
    private MathExtend mathExtend;

    private Map<String,Object> mapValues = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        webView = (WebView) findViewById(R.id.formula_page);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        i = getIntent().getIntExtra("Variation",1);
        mapValues = (HashMap)getIntent().getSerializableExtra("map");

        if(i == 1){
            mathExtend = new GenerateMath(this,mapValues);
        }
        else if(i == 2){
            mathExtend = new GenerateMath2(this,mapValues);
        }
        else if(i == 3){
            mathExtend = new GenerateMath3(this,mapValues);
        }
        else if(i == 4){
            mathExtend = new GenerateMath4(this,mapValues);
        }
        else if(i == 5){
            mathExtend = new GenerateMath5(this,mapValues);
            String images[] = getIntent().getStringArrayExtra("images");

            for(int i=0; i < images.length; i++){
                checkedImages.add(new ImagesObj("",images[i],0));
            }
            mathExtend.setCheckedImages(checkedImages);
        }


        Locale current = getResources().getConfiguration().locale;
        if(current.getLanguage() == "bg")
            mathExtend.language(0);
        else{
            mathExtend.language(1);
        }
        try {
            mathExtend.save(mathExtend.getFile(),mathExtend.createHTML());
        } catch (IOException e) {
            e.printStackTrace();
        }
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(view.GONE);
                webView.setVisibility(view.VISIBLE);
            }
        });
        webView.loadUrl(String.valueOf(mathExtend.getFile().toURI()));



    }


    //@TargetApi(Build.VERSION_CODES.KITKAT)
    private void createWebPrintJob(WebView _webView) {

        PrintManager printManager = (PrintManager) this
                .getSystemService(Context.PRINT_SERVICE);

        PrintDocumentAdapter printAdapter =
                _webView.createPrintDocumentAdapter();
        //String jobName = getString(R.string.app_name) + " Document";
        String jobName = "Calculator Document";

        PrintJob printJob = printManager.print(jobName, printAdapter,
                new PrintAttributes.Builder().build());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.print) {
            this.createWebPrintJob(webView);
            return true;
        }
        else if(id == R.id.HelpSupport){
            AlertDialog alertDialog = new AlertDialog.Builder(WebViewActivity.this).create();
            alertDialog.setTitle("Help and support");
            alertDialog.setMessage("Yuri Rusanov"+System.getProperty("line.separator")+" email: yuri1994@abv.bg"+System.getProperty("line.separator")+"Radost Dukova"+System.getProperty("line.separator")+" email: radostinadukova@abv.bg");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
