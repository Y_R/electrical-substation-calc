package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import application.android.yr.calculator_tu.R;
import application.android.yr.calculator_tu.WebViewActivity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Three extends Fragment implements View.OnClickListener{

    private Map<String, Object> mapValues = new HashMap<>();
    private View view;
    private Button button;
    private Map<String,String> checkFields = new HashMap<>();

    private EditText editText_Iprim_K2_3;
    private EditText editText_I02_K2_3;
    private EditText editText_I8_K2_3;
    private EditText editText_IzklReleinaZasht_3;
    private EditText editText_kp_3;
    private EditText editText_MinIzklReleinaZasht_3;
    private EditText editText_xr_3;
    private EditText editText_A_3;
    private EditText editText_name_3;
    private EditText editText_Un_3;
    private EditText editText_Ipurvchen_3;
    private EditText editText_Ivtorichen_3;
    private EditText editText_Pvtorichen_3;
    private EditText editText_Itermust_3;
    private EditText editText_klasTochnost_3;
    private EditText editText_Rk_3;
    private EditText editText_L_3;
    private EditText editText_ksh_3;
    private EditText editText_kt_3;
    private EditText editText_ro_3;

    private EditText    editText_Ampermetur_FazaB_3    ;
    private EditText    editText_Aktivna_FazaB_4      ;
    private EditText    editText_Reaktivna_FazaB_4    ;
    private EditText    editText_Reaktivna_FazaB_42   ;
    private EditText    editText_Reaktivna_FazaB_43   ;

    public Three() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_three, container, false);
        button = (Button)view.findViewById(R.id.button3);
        button.setOnClickListener(this);
        view.setClickable(true);

        editText_Iprim_K2_3=(EditText)view.findViewById(R.id.editText_Iprim_K2_3);
        editText_I02_K2_3=(EditText)view.findViewById(R.id.editText_I02_K2_3);
        editText_I8_K2_3=(EditText)view.findViewById(R.id.editText_I8_K2_3);
        editText_IzklReleinaZasht_3=(EditText)view.findViewById(R.id.editText_IzklReleinaZasht_3);
        editText_kp_3=(EditText)view.findViewById(R.id.editText_kp_3);
        editText_MinIzklReleinaZasht_3=(EditText)view.findViewById(R.id.editText_MinIzklReleinaZasht_3);
        editText_xr_3=(EditText)view.findViewById(R.id.editText_xr_3);
        editText_A_3=(EditText)view.findViewById(R.id.editText_A_3);
        editText_name_3=(EditText)view.findViewById(R.id.editText_name_3);
        editText_Un_3=(EditText)view.findViewById(R.id.editText_Un_3);
        editText_Ipurvchen_3=(EditText)view.findViewById(R.id.editText_Ipurvchen_3);
        editText_Ivtorichen_3=(EditText)view.findViewById(R.id.editText_Ivtorichen_3);
        editText_Pvtorichen_3=(EditText)view.findViewById(R.id.editText_Pvtorichen_3);
        editText_Itermust_3=(EditText)view.findViewById(R.id.editText_Itermust_3);
        editText_klasTochnost_3=(EditText)view.findViewById(R.id.editText_klasTochnost_3);
        editText_Rk_3=(EditText)view.findViewById(R.id.editText_Rk_3);
        editText_L_3=(EditText)view.findViewById(R.id.editText_L_3);
        editText_ksh_3=(EditText)view.findViewById(R.id.editText_ksh_3);
        editText_kt_3=(EditText)view.findViewById(R.id.editText_kt_3);
        editText_ro_3=(EditText)view.findViewById(R.id.editText_ro_3);
        editText_Ampermetur_FazaB_3 = (EditText)view.findViewById(R.id.editText_Ampermetur_FazaB_3);
        editText_Aktivna_FazaB_4    = (EditText)view.findViewById(R.id.editText_Aktivna_FazaB_4);
        editText_Reaktivna_FazaB_4  = (EditText)view.findViewById(R.id.editText_Reaktivna_FazaB_4);
        editText_Reaktivna_FazaB_42 = (EditText)view.findViewById(R.id.editText_Reaktivna_FazaB_42);
        editText_Reaktivna_FazaB_43 = (EditText)view.findViewById(R.id.editText_Reaktivna_FazaB_43);

        ((TextView)view.findViewById(R.id.textView120)).setText(Html.fromHtml("I<sup>''</sup>"));
        ((TextView)view.findViewById(R.id.textView121)).setText(Html.fromHtml("I<sub>0.2</sub>"));
        ((TextView)view.findViewById(R.id.textView122)).setText(Html.fromHtml("I<sub>∞</sub>"));


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button3:
                Intent intent = new Intent(getActivity(),WebViewActivity.class);
                this.getCheckFields();
                this.getValues();
                intent.putExtra("map",(Serializable)mapValues);
                intent.putExtra("Variation", 3);
                startActivity(intent);
        }
    }

    public void getCheckFields(){
        checkFields.put("editText_Iprim_K2_3",String.valueOf(editText_Iprim_K2_3.getText()));
        checkFields.put("editText_I02_K2_3",String.valueOf(editText_I02_K2_3.getText()));
        checkFields.put("editText_I8_K2_3",String.valueOf(editText_I8_K2_3.getText()));
        checkFields.put("editText_IzklReleinaZasht_3",String.valueOf(editText_IzklReleinaZasht_3.getText()));
        checkFields.put("editText_kp_3",String.valueOf(editText_kp_3.getText()));
        checkFields.put("editText_MinIzklReleinaZasht_3",String.valueOf(editText_MinIzklReleinaZasht_3.getText()));
        checkFields.put("editText_xr_3",String.valueOf(editText_xr_3.getText()));
        checkFields.put("editText_A_3",String.valueOf(editText_A_3.getText()));
        //checkFields.put("editText_name_3",String.valueOf(editText_name_3.getText()));
        checkFields.put("editText_Un_3",String.valueOf(editText_Un_3.getText()));
        checkFields.put("editText_Ipurvchen_3",String.valueOf(editText_Ipurvchen_3.getText()));
        checkFields.put("editText_Ivtorichen_3",String.valueOf(editText_Ivtorichen_3.getText()));
        checkFields.put("editText_Pvtorichen_3",String.valueOf(editText_Pvtorichen_3.getText()));
        checkFields.put("editText_Itermust_3",String.valueOf(editText_Itermust_3.getText()));
        checkFields.put("editText_klasTochnost_3",String.valueOf(editText_klasTochnost_3.getText()));
        checkFields.put("editText_Rk_3",String.valueOf(editText_Rk_3.getText()));
        checkFields.put("editText_L_3",String.valueOf(editText_L_3.getText()));
        checkFields.put("editText_ksh_3",String.valueOf(editText_ksh_3.getText()));
        checkFields.put("editText_kt_3",String.valueOf(editText_kt_3.getText()));
        checkFields.put("editText_ro_3",String.valueOf(editText_ro_3.getText()));

        checkFields.put("editText_Ampermetur_FazaB_3",String.valueOf(editText_Ampermetur_FazaB_3.getText()));
        checkFields.put("editText_Aktivna_FazaB_4",String.valueOf(editText_Aktivna_FazaB_4.getText()));
        checkFields.put("editText_Reaktivna_FazaB_4",String.valueOf(editText_Reaktivna_FazaB_4.getText()));
        checkFields.put("editText_Reaktivna_FazaB_42",String.valueOf(editText_Reaktivna_FazaB_42.getText()));
        checkFields.put("editText_Reaktivna_FazaB_43",String.valueOf(editText_Reaktivna_FazaB_43.getText()));

    }

    public void getValues(){

        for(String key: checkFields.keySet()){
            String str = checkFields.get(key);
            if(str.equals("")){
                mapValues.put(key,0.00);
            }
            else{
                mapValues.put(key,Double.valueOf(checkFields.get(key)));
            }
        }

        mapValues.put("editText_name_3",String.valueOf(editText_name_3.getText()));
    }

}
