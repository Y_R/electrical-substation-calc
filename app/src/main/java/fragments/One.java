package fragments;


import android.app.ActionBar;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import application.android.yr.calculator_tu.R;
import application.android.yr.calculator_tu.WebViewActivity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class One extends Fragment implements View.OnClickListener{


    public One() {
        // Required empty public constructor
    }


    private Map<String, Object> mapValues = new HashMap<>();
    private View view;
    private Button button;

    private EditText editText_Unsrn     ;
    private EditText editText_Unvn      ;
    private EditText editText_Usrednosr ;
    private EditText editText_Usrednovn ;
    private EditText editText_Skobr;
    private EditText editText_NameTrans1;
    private EditText editText_Sn_Trans1 ;
    private EditText editText_Un_Trans1 ;
    private EditText editText_I0_Trans1 ;
    private EditText editText_Uk_Trans1 ;
    private EditText editText_Pst_Trans1;
    private EditText editText_Pcu_Trans1;

    private EditText editText_NameTrans2;
    private EditText editText_Sn_Trans2 ;
    private EditText editText_Un_Trans2 ;
    private EditText editText_I0_Trans2 ;
    private EditText editText_Uk_Trans2 ;
    private EditText editText_Pst_Trans2;
    private EditText editText_Pcu_Trans2;

    private EditText editText_Iprim_K1  ;
    private EditText editText_I02_K1    ;
    private EditText editText_I8_K1     ;

    private EditText editText_Iprim_K2  ;
    private EditText editText_I02_K2    ;
    private EditText editText_I8_K2     ;
    private Map<String,String> checkFields = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, container, false);
        // Inflate the layout for this fragment\
        button = (Button)view.findViewById(R.id.button);
        button.setOnClickListener(this);
        view.setClickable(true);

        ((TextView)view.findViewById(R.id.textView2)).setText(Html.fromHtml("I<sup>''</sup>"));
        ((TextView)view.findViewById(R.id.textView3)).setText(Html.fromHtml("I<sub>0.2</sub>"));
        ((TextView)view.findViewById(R.id.textView4)).setText(Html.fromHtml("I<sub>∞</sub>"));
        ((TextView)view.findViewById(R.id.textView7)).setText(Html.fromHtml("I<sup>''</sup>"));
        ((TextView)view.findViewById(R.id.textView8)).setText(Html.fromHtml("I<sub>0.2</sub>"));
        ((TextView)view.findViewById(R.id.textView9)).setText(Html.fromHtml("I<sub>∞</sub>"));

        editText_Unsrn         = (EditText) view.findViewById(R.id.editText_Unsrn);
        editText_NameTrans1    = (EditText)view.findViewById(R.id.editText_NameTrans1);
        editText_Sn_Trans1     = (EditText)view.findViewById(R.id.editText_Sn_Trans1);
        editText_Un_Trans1     = (EditText)view.findViewById(R.id.editText_Un_Trans1);
        editText_I0_Trans1     = (EditText)view.findViewById(R.id.editText_I0_Trans1);
        editText_Uk_Trans1     = (EditText)view.findViewById(R.id.editText_Uk_Trans1);
        editText_Pst_Trans1    = (EditText)view.findViewById(R.id.editText_Pst_Trans1);
        editText_Pcu_Trans1    = (EditText)view.findViewById(R.id.editText_Pcu_Trans1);

        editText_Unvn          = (EditText)view.findViewById(R.id.editText_Unvn);
        editText_Usrednosr     = (EditText)view.findViewById(R.id.editText_Usrednosr);
        editText_Usrednovn     = (EditText)view.findViewById(R.id.editText_Usrednovn);
        editText_Skobr         = (EditText)view.findViewById(R.id.editText_Skobr);

        editText_NameTrans2    = (EditText)view.findViewById(R.id.editText_NameTrans2);
        editText_Sn_Trans2     = (EditText)view.findViewById(R.id.editText_Sn_Trans2);
        editText_Un_Trans2     = (EditText)view.findViewById(R.id.editText_Un_Trans2);
        editText_I0_Trans2     = (EditText)view.findViewById(R.id.editText_I0_Trans2);
        editText_Uk_Trans2     = (EditText)view.findViewById(R.id.editText_Uk_Trans2);
        editText_Pst_Trans2    = (EditText)view.findViewById(R.id.editText_Pst_Trans2);
        editText_Pcu_Trans2    = (EditText)view.findViewById(R.id.editText_Pcu_Trans2);

        editText_Iprim_K1      = (EditText)view.findViewById(R.id.editText_Iprim_K1);

        editText_I02_K1        = (EditText)view.findViewById(R.id.editText_I02_K1);

        editText_I8_K1         = (EditText)view.findViewById(R.id.editText_I8_K1);

        editText_Iprim_K2      = (EditText)view.findViewById(R.id.editText_Iprim_K2);
        editText_I02_K2        = (EditText)view.findViewById(R.id.editText_I02_K2);
        editText_I8_K2         = (EditText)view.findViewById(R.id.editText_I8_K2);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }
    public void getCheckFields(){
        checkFields.put("editText_Unsrn",String.valueOf(editText_Unsrn.getText()));
        checkFields.put("editText_Sn_Trans1",String.valueOf(editText_Sn_Trans1.getText()));


        checkFields.put("editText_Un_Trans1",String.valueOf(editText_Un_Trans1.getText()));
        checkFields.put("editText_I0_Trans1",String.valueOf(editText_I0_Trans1.getText()));
        checkFields.put("editText_Uk_Trans1",String.valueOf(editText_Uk_Trans1.getText()));
        checkFields.put("editText_Pst_Trans1",String.valueOf(editText_Pst_Trans1.getText()));
        checkFields.put("editText_Pcu_Trans1",String.valueOf(editText_Pcu_Trans1.getText()));
        checkFields.put("editText_Unvn",String.valueOf(editText_Unvn.getText()));
        checkFields.put("editText_Usrednosr",String.valueOf(editText_Usrednosr.getText()));
        checkFields.put("editText_Usrednovn",String.valueOf(editText_Usrednovn.getText()));
        checkFields.put("editText_Skobr",String.valueOf(editText_Skobr.getText()));

        checkFields.put("editText_Sn_Trans2",String.valueOf(editText_Sn_Trans2.getText()));
        checkFields.put("editText_Un_Trans2",String.valueOf(editText_Un_Trans2.getText()));
        checkFields.put("editText_I0_Trans2",String.valueOf(editText_I0_Trans2.getText()));
        checkFields.put("editText_Uk_Trans2",String.valueOf(editText_Uk_Trans2.getText()));
        checkFields.put("editText_Pst_Trans2",String.valueOf(editText_Pst_Trans2.getText()));
        checkFields.put("editText_Pcu_Trans2",String.valueOf(editText_Pcu_Trans2.getText()));

        checkFields.put("editText_Iprim_K1",String.valueOf(editText_Iprim_K1.getText()));
        checkFields.put("editText_I02_K1",String.valueOf(editText_I02_K1.getText()));
        checkFields.put("editText_I8_K1",String.valueOf(editText_I8_K1.getText()));

        checkFields.put("editText_Iprim_K2",String.valueOf(editText_Iprim_K2.getText()));
        checkFields.put("editText_I02_K2",String.valueOf(editText_I02_K2.getText()));
        checkFields.put("editText_I8_K2",String.valueOf(editText_I8_K2.getText()));
    }

    public void getValues(){

        for(String key: checkFields.keySet()){
            String str = checkFields.get(key);
            if(str.equals("")){
                mapValues.put(key,0.00);
            }
            else{
                mapValues.put(key,Double.valueOf(checkFields.get(key)));
            }
        }

        mapValues.put("editText_NameTrans1",String.valueOf(editText_NameTrans1.getText()));
        mapValues.put("editText_NameTrans2",String.valueOf(editText_NameTrans2.getText()));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                Intent intent = new Intent(getActivity(),WebViewActivity.class);
                this.getCheckFields();
                this.getValues();
                intent.putExtra("map",(Serializable)mapValues);
                intent.putExtra("Variation", 1);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
