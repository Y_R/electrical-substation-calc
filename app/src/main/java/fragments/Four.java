package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import application.android.yr.calculator_tu.R;
import application.android.yr.calculator_tu.WebViewActivity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Four extends Fragment implements View.OnClickListener {


    public Four() {
        // Required empty public constructor
    }

    private Map<String, Object> mapValues = new HashMap<>();
    private View view;
    private Button button;
    private Map<String,String> checkFields = new HashMap<>();

    private EditText editText_Voltmetur_Broi_4;
    private EditText editText_Voltmetur_Namotka_4;
    private EditText editText_Voltmetur_FazaA_4;

    private EditText editText_Voltmetur_FazaC_4;
    private EditText editText_Aktivna_Broi_4;
    private EditText editText_Aktivna_Namotka_4;
    private EditText editText_Aktivna_FazaA_4;

    private EditText editText_Aktivna_FazaC_4;
    private EditText editText_Reaktivna_Broi_4;
    private EditText editText_Reaktivna_Namotka_4;
    private EditText editText_Reaktivna_FazaA_4;

    private EditText editText_Reaktivna_FazaC_4;
    private EditText editText_TransName_4;
    private EditText editText_Un_4;
    private EditText editText_KoeficientTrans_4;
    private EditText editText_NominalnaMoshtnost_4;
    private EditText editText_KlasNaTochnost_4;
    private EditText editText_duljina_4;
    private EditText editText_specifichnoSup_4;
    private EditText editText_MaksNapr_4;


    private EditText editText_Voltmetur_FazaB_4 ;
    private EditText editText_Aktivna_FazaB_4   ;
    private EditText editText_Reaktivna_FazaB_4 ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_four, container, false);
        // Inflate the layout for this fragment\
        button = (Button)view.findViewById(R.id.button4);
        button.setOnClickListener(this);
        view.setClickable(true);

        editText_Voltmetur_Broi_4    		= (EditText) view.findViewById(R.id.editText_Voltmetur_Broi_4);
        editText_Voltmetur_Namotka_4 		= (EditText) view.findViewById(R.id.editText_Voltmetur_Namotka_4);
        editText_Voltmetur_FazaA_4          = (EditText) view.findViewById(R.id.editText_Voltmetur_FazaA_4);
        editText_Voltmetur_FazaB_4          = (EditText) view.findViewById(R.id.editText_Voltmetur_FazaB_4);
        editText_Voltmetur_FazaC_4          = (EditText) view.findViewById(R.id.editText_Voltmetur_FazaC_4);
        editText_Aktivna_Broi_4             = (EditText) view.findViewById(R.id.editText_Aktivna_Broi_4);
        editText_Aktivna_Namotka_4          = (EditText) view.findViewById(R.id.editText_Aktivna_Namotka_4);
        editText_Aktivna_FazaA_4            = (EditText) view.findViewById(R.id.editText_Aktivna_FazaA_4);
        editText_Aktivna_FazaB_4            = (EditText) view.findViewById(R.id.editText_Aktivna_FazaB_4);
        editText_Aktivna_FazaC_4            = (EditText) view.findViewById(R.id.editText_Aktivna_FazaC_4);
        editText_Reaktivna_Broi_4           = (EditText) view.findViewById(R.id.editText_Reaktivna_Broi_4);
        editText_Reaktivna_Namotka_4        = (EditText) view.findViewById(R.id.editText_Reaktivna_Namotka_4);
        editText_Reaktivna_FazaA_4          = (EditText) view.findViewById(R.id.editText_Reaktivna_FazaA_4);
        editText_Reaktivna_FazaB_4          = (EditText) view.findViewById(R.id.editText_Reaktivna_FazaB_4);
        editText_Reaktivna_FazaC_4          = (EditText) view.findViewById(R.id.editText_Reaktivna_FazaC_4);
        editText_TransName_4                = (EditText) view.findViewById(R.id.editText_TransName_4);
        editText_Un_4                       = (EditText) view.findViewById(R.id.editText_Un_4);
        editText_KoeficientTrans_4          = (EditText) view.findViewById(R.id.editText_KoeficientTrans_4);
        editText_NominalnaMoshtnost_4       = (EditText) view.findViewById(R.id.editText_NominalnaMoshtnost_4);
        editText_KlasNaTochnost_4           = (EditText) view.findViewById(R.id.editText_KlasNaTochnost_4);
        editText_duljina_4                  = (EditText) view.findViewById(R.id.editText_duljina_4);
        editText_specifichnoSup_4           = (EditText) view.findViewById(R.id.editText_specifichnoSup_4);
        editText_MaksNapr_4                 = (EditText) view.findViewById(R.id.editText_MaksNapr_4);

        editText_Voltmetur_FazaB_4          = (EditText) view.findViewById(R.id.editText_Voltmetur_FazaB_4);
        editText_Aktivna_FazaB_4            = (EditText) view.findViewById(R.id.editText_Aktivna_FazaB_4);
        editText_Reaktivna_FazaB_4          = (EditText) view.findViewById(R.id.editText_Reaktivna_FazaB_4);

        return view;
    }

    public void getCheckFields(){
        checkFields.put("editText_Voltmetur_Broi_4",String.valueOf(editText_Voltmetur_Broi_4.getText()));
        checkFields.put("editText_Voltmetur_Namotka_4",String.valueOf(editText_Voltmetur_Namotka_4.getText()));
        checkFields.put("editText_Voltmetur_FazaA_4",String.valueOf(editText_Voltmetur_FazaA_4.getText()));
        checkFields.put("editText_Voltmetur_FazaB_4",String.valueOf(editText_Voltmetur_FazaB_4.getText()));
        checkFields.put("editText_Voltmetur_FazaC_4",String.valueOf(editText_Voltmetur_FazaC_4.getText()));
        checkFields.put("editText_Aktivna_Broi_4",String.valueOf(editText_Aktivna_Broi_4.getText()));
        checkFields.put("editText_Aktivna_Namotka_4",String.valueOf(editText_Aktivna_Namotka_4.getText()));
        checkFields.put("editText_Aktivna_FazaA_4",String.valueOf(editText_Aktivna_FazaA_4.getText()));
        checkFields.put("editText_Aktivna_FazaB_4",String.valueOf(editText_Aktivna_FazaB_4.getText()));
        checkFields.put("editText_Aktivna_FazaC_4",String.valueOf(editText_Aktivna_FazaC_4.getText()));
        checkFields.put("editText_Reaktivna_Broi_4",String.valueOf(editText_Reaktivna_Broi_4.getText()));
        checkFields.put("editText_Reaktivna_Namotka_4",String.valueOf(editText_Reaktivna_Namotka_4.getText()));
        checkFields.put("editText_Reaktivna_FazaA_4",String.valueOf(editText_Reaktivna_FazaA_4.getText()));
        checkFields.put("editText_Reaktivna_FazaB_4",String.valueOf(editText_Reaktivna_FazaB_4.getText()));
        checkFields.put("editText_Reaktivna_FazaC_4",String.valueOf(editText_Reaktivna_FazaC_4.getText()));
        //checkFields.put("editText_TransName_4",String.valueOf(editText_TransName_4.getText()));
        checkFields.put("editText_Un_4",String.valueOf(editText_Un_4.getText()));
        //checkFields.put("editText_KoeficientTrans_4",String.valueOf(editText_KoeficientTrans_4.getText()));
        checkFields.put("editText_NominalnaMoshtnost_4",String.valueOf(editText_NominalnaMoshtnost_4.getText()));
        checkFields.put("editText_KlasNaTochnost_4",String.valueOf(editText_KlasNaTochnost_4.getText()));
        checkFields.put("editText_duljina_4",String.valueOf(editText_duljina_4.getText()));
        checkFields.put("editText_specifichnoSup_4",String.valueOf(editText_specifichnoSup_4.getText()));
        checkFields.put("editText_MaksNapr_4",String.valueOf(editText_MaksNapr_4.getText()));


        checkFields.put("editText_Voltmetur_FazaB_4",String.valueOf(editText_Voltmetur_FazaB_4.getText()));
        checkFields.put("editText_Aktivna_FazaB_4",String.valueOf(editText_Aktivna_FazaB_4.getText()));
        checkFields.put("editText_Reaktivna_FazaB_4",String.valueOf(editText_Reaktivna_FazaB_4.getText()));

    }

    public void getValues(){

        for(String key: checkFields.keySet()){
            String str = checkFields.get(key);
            if(str.equals("")){
                mapValues.put(key,0.00);
            }
            else{
                mapValues.put(key,Double.valueOf(checkFields.get(key)));
            }
        }

        mapValues.put("editText_TransName_4",String.valueOf(editText_TransName_4.getText()));
        mapValues.put("editText_KoeficientTrans_4",String.valueOf(editText_KoeficientTrans_4.getText()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button4:
                Intent intent = new Intent(getActivity(),WebViewActivity.class);
                this.getCheckFields();
                this.getValues();
                intent.putExtra("map",(Serializable)mapValues);
                intent.putExtra("Variation", 4);
                startActivity(intent);
                break;
            default:
                break;
        }

    }
}
