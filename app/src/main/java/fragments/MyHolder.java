package fragments;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import application.android.yr.calculator_tu.R;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    ImageView img;
    TextView nameTxt, posTxt;
    CheckBox chk;

    ItemClickListener itemClickListener;


    public MyHolder(View itemView) {
        super(itemView);

        nameTxt = (TextView) itemView.findViewById(R.id.item_title);
        img = (ImageView)itemView.findViewById(R.id.item_image);
        chk = (CheckBox)itemView.findViewById(R.id.checkBox);


        chk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v,getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}
