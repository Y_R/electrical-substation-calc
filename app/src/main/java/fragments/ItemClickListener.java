package fragments;

import android.view.View;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public interface ItemClickListener {

    void onItemClick(View v, int pos);

}
