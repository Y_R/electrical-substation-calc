package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import application.android.yr.calculator_tu.R;
import application.android.yr.calculator_tu.WebViewActivity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Two extends Fragment implements View.OnClickListener {


    private EditText editText_Iprim_K2;
    private EditText editText_I02_K2;
    private EditText editText_I8_K2;
    private EditText editText_Unsrn;
    private EditText editText_Unvn;
    private EditText editText_Usrednosr;
    private EditText editText_Usrednovn;
    private EditText editText_Skobr;
    private EditText editText_vremeZashtitaIzkliuchvane;
    private EditText editText_kpr;
    private EditText editText_vremeZashtitaMinimalno;
    private EditText editText_xr;
    private EditText editText_A;
    private EditText editText_NamePrekasvach;
    private EditText editText_Un;
    private EditText editText_In;
    private EditText editText_Inomizk;
    private EditText editText_Itermust;
    private EditText editText_ttermust;
    private EditText editText_Ivkl;
    private EditText editText_tizkl;
    private EditText editText_tduga;
    private EditText editText_beta;
    private EditText editText_NameRazedinitel;
    private EditText editText_Unom;
    private EditText editText_Inom;
    private EditText editText_Idin;
    private EditText editText_Itermust_raz;
    private EditText editText_tterm_raz;

    private Map<String,String> checkFields = new HashMap<>();
    private Map<String, Object> mapValues = new HashMap<>();

    public Two() {
        // Required empty public constructor
    }
    private Button button;
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_two, container, false);
        button = (Button)view.findViewById(R.id.button2);
        button.setOnClickListener(this);
        view.setClickable(true);

        editText_Iprim_K2                  = (EditText)view.findViewById(R.id.editText_Iprim_K2_2);

        editText_I02_K2                    = (EditText)view.findViewById(R.id.editText_I02_K2_2);
        editText_I8_K2                     = (EditText)view.findViewById(R.id.editText_I8_K2_2);
        editText_Unsrn                     = (EditText)view.findViewById(R.id.editText_Unsrn_2);
        editText_Unvn                      = (EditText)view.findViewById(R.id.editText_Unvn_2);
        editText_Usrednosr                 = (EditText)view.findViewById(R.id.editText_Usrednosr_2);
        editText_Usrednovn                 = (EditText)view.findViewById(R.id.editText_Usrednovn_2);
        editText_Skobr                     = (EditText)view.findViewById(R.id.editText_Skobr_2);
        editText_vremeZashtitaIzkliuchvane = (EditText)view.findViewById(R.id.editText_vremeZashtitaIzkliuchvane_2);
        editText_kpr                       = (EditText)view.findViewById(R.id.editText_kpr_2);
        editText_vremeZashtitaMinimalno    = (EditText)view.findViewById(R.id.editText_vremeZashtitaMinimalno_2);
        editText_xr                        = (EditText)view.findViewById(R.id.editText_xr_2);
        editText_A                         = (EditText)view.findViewById(R.id.editText_A);
        editText_NamePrekasvach            = (EditText)view.findViewById(R.id.editText_NamePrekasvach_2);
        editText_Un                        = (EditText)view.findViewById(R.id.editText_Un_2);
        editText_In                        = (EditText)view.findViewById(R.id.editText_In_2);
        editText_Inomizk                   = (EditText)view.findViewById(R.id.editText_Inomizk_2);
        editText_Itermust                  = (EditText)view.findViewById(R.id.editText_Itermust_2);
        editText_ttermust                  = (EditText)view.findViewById(R.id.editText_ttermust_2);
        editText_Ivkl                      = (EditText)view.findViewById(R.id.editText_Ivkl_2);
        editText_tizkl                     = (EditText)view.findViewById(R.id.editText_tizkl_2);
        editText_tduga                     = (EditText)view.findViewById(R.id.editText_tduga_2);
        editText_beta                      = (EditText)view.findViewById(R.id.editText_beta_2);
        editText_NameRazedinitel           = (EditText)view.findViewById(R.id.editText_NameRazedinitel_2);
        editText_Unom                      = (EditText)view.findViewById(R.id.editText_Unom_2);
        editText_Inom                      = (EditText)view.findViewById(R.id.editText_Inom_2);
        editText_Idin                      = (EditText)view.findViewById(R.id.editText_Idin_2);
        editText_Itermust_raz              = (EditText)view.findViewById(R.id.editText_Itermust_raz_2);
        editText_tterm_raz                 = (EditText)view.findViewById(R.id.editText_tterm_raz_2);



        ((TextView)view.findViewById(R.id.textView33)).setText(Html.fromHtml("I<sup>''</sup>"));
        ((TextView)view.findViewById(R.id.textView34)).setText(Html.fromHtml("I<sub>0.2</sub>"));
        ((TextView)view.findViewById(R.id.textView35)).setText(Html.fromHtml("I<sub>∞</sub>"));


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button2:
                Intent intent = new Intent(getActivity(),WebViewActivity.class);
                this.getCheckFields();
                this.getValues();
                intent.putExtra("map",(Serializable)mapValues);
                intent.putExtra("Variation", 2);
                startActivity(intent);
                break;
            default:
                break;
        }
    }


    public void getCheckFields(){

        checkFields.put("editText_Iprim_K2"                 ,String.valueOf(editText_Iprim_K2.getText()));
        checkFields.put("editText_I02_K2"                   ,String.valueOf(editText_I02_K2.getText()));
        checkFields.put("editText_I8_K2"                    ,String.valueOf(editText_I8_K2.getText()));
        checkFields.put("editText_Unsrn"                    ,String.valueOf(editText_Unsrn.getText()));
        checkFields.put("editText_Unvn"                     ,String.valueOf(editText_Unvn.getText()));
        checkFields.put("editText_Usrednosr"                ,String.valueOf(editText_Usrednosr.getText()));
        checkFields.put("editText_Usrednovn"                ,String.valueOf(editText_Usrednovn.getText()));
        checkFields.put("editText_Skobr"                    ,String.valueOf(editText_Skobr.getText()));
        checkFields.put("editText_vremeZashtitaIzkliuchvane",String.valueOf(editText_vremeZashtitaIzkliuchvane.getText()));
        checkFields.put("editText_kpr"                      ,String.valueOf(editText_kpr.getText()));
        checkFields.put("editText_vremeZashtitaMinimalno"  ,String.valueOf(editText_vremeZashtitaMinimalno.getText()));
        checkFields.put("editText_xr"                       ,String.valueOf(editText_xr.getText()));
        checkFields.put("editText_A"                        ,String.valueOf(editText_A.getText()));
        //checkFields.put("editText_NamePrekasvach"           ,String.valueOf(editText_NamePrekasvach.getText()));
        checkFields.put("editText_Un"                       ,String.valueOf(editText_Un.getText()));
        checkFields.put("editText_In"                       ,String.valueOf(editText_In.getText()));
        checkFields.put("editText_Inomizk"                  ,String.valueOf(editText_Inomizk.getText()));
        checkFields.put("editText_Itermust"                 ,String.valueOf(editText_Itermust.getText()));
        checkFields.put("editText_ttermust"                 ,String.valueOf(editText_ttermust.getText()));
        checkFields.put("editText_Ivkl"                     ,String.valueOf(editText_Ivkl.getText()));
        checkFields.put("editText_tizkl"                    ,String.valueOf(editText_tizkl.getText()));
        checkFields.put("editText_tduga"                    ,String.valueOf(editText_tduga.getText()));
        checkFields.put("editText_beta"                     ,String.valueOf(editText_beta.getText()));
        //checkFields.put("editText_NameRazedinitel"          ,String.valueOf(editText_NameRazedinitel.getText()));
        checkFields.put("editText_Inom"                     ,String.valueOf(editText_Inom.getText()));

        checkFields.put("editText_Unom"                     ,String.valueOf(editText_Unom.getText()));

        checkFields.put("editText_Itermust_raz"             ,String.valueOf(editText_Itermust_raz.getText()));

        checkFields.put("editText_Idin"                     ,String.valueOf(editText_Idin.getText()));

        checkFields.put("editText_tterm_raz"                ,String.valueOf(editText_tterm_raz.getText()));

    }

    public void getValues(){

        for(String key: checkFields.keySet()){
            String str = checkFields.get(key);
            if(str.equals("")){
                mapValues.put(key,0.00);
            }
            else{
                mapValues.put(key,Double.valueOf(checkFields.get(key)));
            }
        }

        mapValues.put("editText_NameRazedinitel",String.valueOf(editText_NameRazedinitel.getText()));
        mapValues.put("editText_NamePrekasvach",String.valueOf(editText_NamePrekasvach.getText()));
    }

}
