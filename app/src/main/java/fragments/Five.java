package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import application.android.yr.calculator_tu.R;
import application.android.yr.calculator_tu.WebViewActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Five extends Fragment implements View.OnClickListener{


    public Five() {
        // Required empty public constructor
    }

    MyAdapter adapter;

    private EditText editText_Iprim_K2_5;
    private EditText editText_I02_K2_5;
    private EditText editText_I8_K2_5;
    private EditText editText_MaksVremeIzklRel_5;
    private EditText editText_kpr_5;
    private EditText editText_MinVremeIzklRel_5;
    private EditText editText_xr_5;
    private EditText editText_A_5;
    private EditText editText_MaksU_5;
    private EditText editText_name_5;
    private EditText editText_Unom_5;
    private EditText editText_Inom_5;
    private EditText editText_Idin_5;
    private EditText editText_Itermust_5;
    private EditText editText_ttermust_5;

    private Map<String, Object> mapValues = new HashMap<>();
    private View view;
    private Button button;
    private Map<String,String> checkFields = new HashMap<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_five, container, false);
        // Inflate the layout for this fragment\

        editText_Iprim_K2_5 = (EditText)view.findViewById(R.id.editText_Iprim_K2_5);
        editText_I02_K2_5 = (EditText)view.findViewById(R.id.editText_I02_K2_5);
        editText_I8_K2_5 = (EditText)view.findViewById(R.id.editText_I8_K2_5);
        editText_MaksVremeIzklRel_5 = (EditText)view.findViewById(R.id.editText_MaksVremeIzklRel_5);
        editText_kpr_5 = (EditText)view.findViewById(R.id.editText_kpr_5);
        editText_MinVremeIzklRel_5 = (EditText)view.findViewById(R.id.editText_MinVremeIzklRel_5);
        editText_xr_5 = (EditText)view.findViewById(R.id.editText_xr_5);
        editText_A_5 = (EditText)view.findViewById(R.id.editText_A_5);
        editText_MaksU_5 = (EditText)view.findViewById(R.id.editText_MaksU_5);
        editText_name_5 = (EditText)view.findViewById(R.id.editText_name_5);
        editText_Unom_5 = (EditText)view.findViewById(R.id.editText_Unom_5);
        editText_Inom_5 = (EditText)view.findViewById(R.id.editText_Inom_5);
        editText_Idin_5 = (EditText)view.findViewById(R.id.editText_Idin_5);
        editText_Itermust_5 = (EditText)view.findViewById(R.id.editText_Itermust_5);
        editText_ttermust_5 = (EditText)view.findViewById(R.id.editText_ttermust_5);

        button = (Button)view.findViewById(R.id.button5);
        button.setOnClickListener(this);
        view.setClickable(true);

        adapter = new MyAdapter(this.getContext(),getImages());

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.recycler_view);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        //rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
        rv.setLayoutManager(new GridLayoutManager(this.getContext(),1));


        ((TextView)view.findViewById(R.id.textView97)).setText(Html.fromHtml("I<sup>''</sup>"));
        ((TextView)view.findViewById(R.id.textView98)).setText(Html.fromHtml("I<sub>0.2</sub>"));
        ((TextView)view.findViewById(R.id.textView99)).setText(Html.fromHtml("I<sub>∞</sub>"));

        return view;

    }

    private ArrayList<ImagesObj> getImages() {
        ArrayList<ImagesObj> images = new ArrayList<>();
        Locale current = getResources().getConfiguration().locale;
        if(current.getLanguage() == "bg"){
            images.add(new ImagesObj("Токов трансформатор","tokov_trans.jpg",R.drawable.tokov_trans));
            images.add(new ImagesObj("Напреженов трансформатор","naprejenov_trans.jpg",R.drawable.naprejenov_trans));
            images.add(new ImagesObj("Заземителен нож на шинна система","zazemitelen_noj_shina.jpg",R.drawable.zazemitelen_noj_shina));
            images.add(new ImagesObj("Кабелна проходка","kabelna_prohodka.jpg",R.drawable.kabelna_prohodka));
            images.add(new ImagesObj("Твърда шинна връзка","tvurda_shinna.jpg",R.drawable.tvurda_shinna));
            images.add(new ImagesObj("Указател за наличие на напрежение","ukazatel_nalichie_shina.jpg",R.drawable.ukazatel_nalichie_shina));
            images.add(new ImagesObj("Токов трансформатор","tokov_trans2.jpg",R.drawable.tokov_trans2));
            images.add(new ImagesObj("Напреженов трансформатор","naprejenov_trans2.jpg",R.drawable.naprejenov_trans2));
            images.add(new ImagesObj("Заземителен нож на шинна система","zazemitelen_noj_shina2.jpg",R.drawable.zazemitelen_noj_shina2));
            images.add(new ImagesObj("Указател за наличие на напрежение","ukazatel_nalichie_shina2.jpg",R.drawable.ukazatel_nalichie_shina2));
        }
        else{
            images.add(new ImagesObj("Current transformer","tokov_trans.jpg",R.drawable.tokov_trans));
            images.add(new ImagesObj("Potential transformer","naprejenov_trans.jpg",R.drawable.naprejenov_trans));
            images.add(new ImagesObj("Make-proof earthing switch","zazemitelen_noj_shina.jpg",R.drawable.zazemitelen_noj_shina));
            images.add(new ImagesObj("Cable sealing ends","kabelna_prohodka.jpg",R.drawable.kabelna_prohodka));
            images.add(new ImagesObj("Bar feeder","tvurda_shinna.jpg",R.drawable.tvurda_shinna));
            images.add(new ImagesObj("Capacitive voltage detecting system","ukazatel_nalichie_shina.jpg",R.drawable.ukazatel_nalichie_shina));
            images.add(new ImagesObj("Current transformer","tokov_trans2.jpg",R.drawable.tokov_trans2));
            images.add(new ImagesObj("Potential transformer","naprejenov_trans2.jpg",R.drawable.naprejenov_trans2));
            images.add(new ImagesObj("Make-proof earthing switch","zazemitelen_noj_shina2.jpg",R.drawable.zazemitelen_noj_shina2));
            images.add(new ImagesObj("Capacitive voltage detecting system","ukazatel_nalichie_shina2.jpg",R.drawable.ukazatel_nalichie_shina2));
        }

        return images;

    }

    StringBuilder sb;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button5:
                Intent intent = new Intent(getActivity(),WebViewActivity.class);
                this.getCheckFields();
                this.getValues();
                String[] d = new String[adapter.checkedImages.size()];
                int i=0;
                for(ImagesObj imagesObj : adapter.checkedImages){
                    d[i] = imagesObj.getPosition();
                    i++;
                }
                intent.putExtra("map",(Serializable)mapValues);
                intent.putExtra("images",d);
                intent.putExtra("Variation", 5);
                startActivity(intent);
        }
    }

    public void getCheckFields(){
        checkFields.put("editText_Iprim_K2_5",String.valueOf(editText_Iprim_K2_5.getText()));
        checkFields.put("editText_I02_K2_5",String.valueOf(editText_I02_K2_5.getText()));
        checkFields.put("editText_I8_K2_5",String.valueOf(editText_I8_K2_5.getText()));
        checkFields.put("editText_MaksVremeIzklRel_5",String.valueOf(editText_MaksVremeIzklRel_5.getText()));
        checkFields.put("editText_kpr_5",String.valueOf(editText_kpr_5.getText()));
        checkFields.put("editText_MinVremeIzklRel_5",String.valueOf(editText_MinVremeIzklRel_5.getText()));
        checkFields.put("editText_xr_5",String.valueOf(editText_xr_5.getText()));
        checkFields.put("editText_A_5",String.valueOf(editText_A_5.getText()));
        checkFields.put("editText_MaksU_5",String.valueOf(editText_MaksU_5.getText()));
        //checkFields.put("editText_name_5",String.valueOf(editText_name_5.getText()));
        checkFields.put("editText_Unom_5",String.valueOf(editText_Unom_5.getText()));
        checkFields.put("editText_Inom_5",String.valueOf(editText_Inom_5.getText()));
        checkFields.put("editText_Idin_5",String.valueOf(editText_Idin_5.getText()));
        checkFields.put("editText_Itermust_5",String.valueOf(editText_Itermust_5.getText()));
        checkFields.put("editText_ttermust_5",String.valueOf(editText_ttermust_5.getText()));
    }

    public void getValues(){

        for(String key: checkFields.keySet()){
            String str = checkFields.get(key);
            if(str.equals("")){
                mapValues.put(key,0.00);
            }
            else{
                mapValues.put(key,Double.valueOf(checkFields.get(key)));
            }
        }

        mapValues.put("editText_name_5",String.valueOf(editText_name_5.getText()));
    }


}
