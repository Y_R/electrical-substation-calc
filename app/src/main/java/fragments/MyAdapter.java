package fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import application.android.yr.calculator_tu.R;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class MyAdapter extends RecyclerView.Adapter<MyHolder> {


    Context context;
    ArrayList<ImagesObj> images;
    ArrayList<ImagesObj> checkedImages = new ArrayList<>();

    public MyAdapter(Context _context, ArrayList<ImagesObj> _images){
        this.context = _context;
        this.images = _images;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,null);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, final int position) {
        holder.nameTxt.setText(images.get(position).getName());
        holder.img.setImageResource(images.get(position).getImgId());
        holder.chk.setOnCheckedChangeListener(null);
        holder.chk.setChecked(images.get(position).isSelectImg());

        holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                images.get(position).setSelectImg(isChecked);
            }
        });

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                CheckBox checkBox = (CheckBox)v;

                if(checkBox.isChecked()){
                    checkedImages.add(images.get(pos));
                }
                else if(!checkBox.isChecked()){
                    checkedImages.remove(images.get(pos));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }
}
