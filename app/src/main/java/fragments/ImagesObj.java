package fragments;

/**
 * Created by y.rusanov on 2016-12-6.
 */

public class ImagesObj {

    String name,position;
    boolean selectImg;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getImgId() {
        return imgId;

    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    int imgId;

    public boolean isSelectImg() {
        return selectImg;
    }

    public void setSelectImg(boolean selectImg) {
        this.selectImg = selectImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ImagesObj(String _name, int _imgId){
        name = _name;
        imgId = _imgId;
    }

    public ImagesObj(String _name, int _imgId, boolean _selected){
        name = _name;
        imgId = _imgId;
        selectImg = _selected;
    }

    public ImagesObj(String _name,String _position, int _imgId){
        name = _name;
        position = _position;
        imgId = _imgId;
    }



}
